import { Component, NgZone, OnInit, OnDestroy } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

import firebase = require("nativescript-plugin-firebase");

//------------------------------------------------------------------------------
//
//  Class
//
//------------------------------------------------------------------------------

@Component({
  selector: "ns-app",
  templateUrl: "app.component.html",
})
export class AppComponent implements OnInit, OnDestroy {

  //--------------------------------------
  //  Properties
  //--------------------------------------

  private authStateChangesListener = {
    onAuthStateChanged: (data) => {
      console.log('<< authStateChanged ~ >>');
      console.dir(data);
      console.log('<< ~ authStateChanged >>');

      if (data.loggedIn) {
        if (data.user && data.user.uid) {
          this.navigateWithCurrentUserState(data.user);
        }
      } else {
        this.ngZone.run(() => {
          this.routerExtensions.navigate(['/start-up/join'], {
            clearHistory: true,
            transition: {
              name: 'flip'
            }
          });
        });
      }
    },
    thisArg: this
  };

  //--------------------------------------
  //  Constructor
  //--------------------------------------

  constructor(
    private ngZone: NgZone,
    private routerExtensions: RouterExtensions
  ) {
    if (!firebase.hasAuthStateListener(this.authStateChangesListener)) {
      firebase.addAuthStateListener(this.authStateChangesListener);
    }

    firebase.getCurrentUser().then((user) => {
      console.log('<< currentUser ~ >>');
      console.dir(user);
      console.log('<< ~ currentUser >>');

      this.navigateWithCurrentUserState(user);
    }, (error) => {
      console.error(error);

      this.ngZone.run(() => {
        this.routerExtensions.navigate(['/start-up/join'], { clearHistory: true });
      });
    });
  }

  //--------------------------------------
  //  Lifecycle hooks
  //--------------------------------------

  ngOnInit() {
    console.log('<< ngInit >>');
  }

  ngOnDestroy() {
    console.log('<< ngDestroy >>');
  }

  //--------------------------------------
  //  Methods
  //--------------------------------------

  private navigateWithCurrentUserState = (user) => {
    // Check user profile filled or not
    firebase.getValue(`/users/${user.uid}/profile`).then((userProfileResult) => {
      console.log('<< currentUser profile ~ >>');
      console.dir(userProfileResult.value);
      console.log('<< ~ currentUser profile >>');

      if (!user.anonymous && userProfileResult.value) {
        // Check user interests choosen or not
        firebase.getValue(`/userInterests/${user.uid}`).then((userInterestsResult) => {
          console.log('<< currentUser interests ~ >>');
          console.dir(userInterestsResult.value);
          console.log('<< ~ currentUser interests >>');

          if (!user.anonymous && userInterestsResult.value) {
            this.ngZone.run(() => {
              this.routerExtensions.navigate(['/main'], {
                clearHistory: true,
                transition: {
                  name: 'flip'
                }
              });
            });
          } else {
            this.ngZone.run(() => {
              this.routerExtensions.navigate(['/choose-interests'], { clearHistory: true });
            });
          }
        });
      } else {
        this.ngZone.run(() => {
          this.routerExtensions.navigate(['/edit-profile'], { clearHistory: true });
        });
      }
    });
  }

}
