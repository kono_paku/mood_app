import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

// --- The built-in 'nativescript-pro-ui' modules, if you are not using 'lazy' loading, uncomment and import the below modules into the 'imports' of the first ngModule (AppModule) of the app.
// import { NativeScriptUISideDrawerModule } from "nativescript-pro-ui/sidedrawer/angular";
import { NativeScriptUIListViewModule } from "nativescript-pro-ui/listview/angular";
// import { NativeScriptUICalendarModule } from "nativescript-pro-ui/calendar/angular";
// import { NativeScriptUIChartModule } from "nativescript-pro-ui/chart/angular";
// import { NativeScriptUIDataFormModule } from "nativescript-pro-ui/dataform/angular";
// import { NativeScriptUIAutoCompleteTextViewModule } from "nativescript-pro-ui/autocomplete/angular";
import { NativeScriptUIGaugesModule } from "nativescript-pro-ui/gauges/angular";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

import { NgShadowModule } from "nativescript-ng-shadow";

import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";

import { StartUpComponent } from "./pages/start-up/start-up.component";
import { JoinComponent } from "./pages/start-up/join/join.component";

import { MainComponent } from "./pages/main/main.component";
import { HomeComponent } from "./pages/main/home/home.component";
import { PostsTimelineComponent } from "./pages/main/home/posts-timeline/posts-timeline.component";

import { SearchComponent } from "./pages/main/search/search.component";
import { SearchBarComponent } from "./pages/main/search/search-bar/search-bar.component";
import { SearchSettingsComponent } from "./pages/main/search/search-settings/search-settings.component";
import { SearchNearPostsComponent } from "./pages/main/search/search-near-posts/search-near-posts.component";

import { ActivityComponent } from "./pages/main/activity/activity.component";
import { MyActivitiesComponent } from "./pages/main/activity/my-activities/my-activities.component";
import { OtherActivitiesComponent } from "./pages/main/activity/other-activities/other-activities.component";
import { MyPageComponent } from "./pages/main/my-page/my-page.component";
import { MyPostsComponent } from "./pages/main/my-page/my-posts/my-posts.component";
import { MyFavoritesComponent } from "./pages/main/my-page/my-favorites/my-favorites.component";
import { MyProfileComponent } from "./pages/main/my-page/my-profile/my-profile.component";

import { PostDetailComponent } from "./pages/post-detail/post-detail.component";
import { PostLikesComponent } from "./pages/post-likes/post-likes.component";
import { PostCommentsComponent } from "./pages/post-comments/post-comments.component";

import { UserProfileComponent } from "./pages/user-profile/user-profile.component";

import { ShootVideoComponent } from "./pages/shoot-video/shoot-video.component";
import { EditPostComponent } from "./pages/edit-post/edit-post.component";

import { EditProfileComponent } from "./pages/edit-profile/edit-profile.component";
import { ChooseInterestsComponent } from "./pages/choose-interests/choose-interests.component";

import { SettingsComponent } from "./pages/settings/settings.component";

import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { CommentService } from "./shared/comment/comment.service";
import { LikeService } from "./shared/like/like.service";
import { PostService } from "./shared/post/post.service";
import { UserService } from "./shared/user/user.service";
import { LocationService } from "./shared/location/location.service";
import { MapApiService } from "./shared/google/map-api.service";
import { HttpService } from "./shared/http/http.service";

import { DateFormatPipe } from "./shared/date-format.pipe";
import { IconFontStringPipe } from "./shared/icon-font-string.pipe";
import { ActivityDescriptionPipe } from "./shared/activity-description.pipe";
import { ActivityUserFormerPipe } from "./shared/activity-user-former.pipe";
import { ActivityUserLatterPipe } from "./shared/activity-user-latter.pipe";

import { registerElement } from "nativescript-angular/element-registry";

registerElement("VideoPlayer", () => require("nativescript-videoplayer").Video);

registerElement(
  "AdvancedVideoView",
  () => require("nativescript-videorecorder/advanced").AdvancedVideoView
);

// Uncomment and add to NgModule imports if you need to use two-way binding
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { LabelMaxLinesDirective } from "./shared/label-max-lines.directive";

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpModule } from 'nativescript-angular/http';

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule,
    NativeScriptUIGaugesModule,
    NativeScriptHttpClientModule,
    AppRoutingModule,
    NgShadowModule
  ],
  declarations: [
    AppComponent,
    StartUpComponent,
    JoinComponent,
    MainComponent,
    HomeComponent,
    PostsTimelineComponent,
    SearchComponent,
    SearchBarComponent,
    SearchNearPostsComponent,
    SearchSettingsComponent,
    ActivityComponent,
    MyActivitiesComponent,
    OtherActivitiesComponent,
    MyPageComponent,
    MyPostsComponent,
    MyFavoritesComponent,
    MyProfileComponent,
    PostDetailComponent,
    PostLikesComponent,
    PostCommentsComponent,
    UserProfileComponent,
    ShootVideoComponent,
    EditPostComponent,
    EditProfileComponent,
    ChooseInterestsComponent,
    SettingsComponent,
    LabelMaxLinesDirective,
    DateFormatPipe,
    IconFontStringPipe,
    ActivityDescriptionPipe,
    ActivityUserFormerPipe,
    ActivityUserLatterPipe
  ],
  providers: [
    ModalDialogService,
    CommentService,
    LikeService,
    UserService,
    PostService,
    LocationService,
    MapApiService,
    HttpService
  ],
  schemas: [NO_ERRORS_SCHEMA]
})

/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule {}
