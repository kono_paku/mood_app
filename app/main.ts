// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScriptDynamic } from "nativescript-angular/platform";

import { AppModule } from "./app.module";

import firebase = require("nativescript-plugin-firebase");

declare var IQKeyboardManager: any;

// Initialize Firebase
firebase.init().then((instance) => {
  console.log(`Firebase init done`);
}, (error) => {
  console.log(`Firebase init error: ${error}`);
});

// Initialize IQKeyboardManager (https://www.npmjs.com/package/nativescript-iqkeyboardmanager)
const iqKeyboard = IQKeyboardManager.sharedManager();
iqKeyboard.toolbarDoneBarButtonItemText = 'Close';
iqKeyboard.shouldResignOnTouchOutside = true;

// A traditional NativeScript application starts by initializing global objects, setting up global CSS rules, creating, and navigating to the main page. 
// Angular applications need to take care of their own initialization: modules, components, directives, routes, DI providers. 
// A NativeScript Angular app needs to make both paradigms work together, so we provide a wrapper platform object, platformNativeScriptDynamic, 
// that sets up a NativeScript application and can bootstrap the Angular framework.
platformNativeScriptDynamic().bootstrapModule(AppModule);
