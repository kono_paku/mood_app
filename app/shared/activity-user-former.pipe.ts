import { Pipe, PipeTransform } from '@angular/core';
import { Constant } from "../app.constant";

/**
 * Convert number value of tpye into string one of activity
 * 
 * @param type number
 * @returns string
 */
@Pipe({ name: 'activityUserFormerPipe' })
export class ActivityUserFormerPipe implements PipeTransform {
  transform(activityUser: any): string {
    // Set initial value for activityUserFormerPipe
    let pipeOutput = "";

    if(activityUser.length < Constant.THREE_INT) {
      pipeOutput = activityUser[Constant.ZERO_INT].name;
    } else if (activityUser.length >= Constant.THREE_INT){
      pipeOutput = activityUser[Constant.ZERO_INT].name + ", " + activityUser[Constant.ONE_INT].name;
    } else {
      console.log("<< Unexpected Error at ActivityUserFormerPipe ~ >>");
      console.log(activityUser.length);
      console.log("<< ~ Unexpected Error at ActivityUserFormerPipe >>");
    }

    return pipeOutput;
  }
}