import * as moment from 'moment';
import { unitOfTime } from 'moment';
import { Pipe, PipeTransform } from '@angular/core';
import { Constant } from "../app.constant";

/**
 * Convert number value of timestamp into string one of date format
 * If the value of date is the same with today or yesterday,
 * fixed text values like 'Today' or 'Yesterday' will be returned.
 * Otherwise, the date value of 'M.DD.YYYY' format will be.
 * 
 * @param timestamp number
 * @returns string
 */
@Pipe({ name: 'dateFormatPipe' })
export class DateFormatPipe implements PipeTransform {
  transform(timestamp: number): string {
    // Set initial value for DateFormatPipe
    let pipeOutput = "";
    
    // Set value for comparison between date
    let todayDate = moment().
                    startOf(Constant.DAY_TYPE_STRING);
    let yesterdayDate = moment().
                        startOf(Constant.DAY_TYPE_STRING).
                        subtract(Constant.YESTERDAY_INDEX_INT, Constant.DAY_TYPE_STRING);
    let createdDate = moment.unix(timestamp / Constant.MILLISECONDS_CONVERTING_VALUE_INT);

    // Check whether the date of today or yesterday, and return the proper date value
    if (todayDate.isSame(createdDate, Constant.DAY_TYPE_STRING)) {
      pipeOutput = Constant.TODAY_STRING;
    } else if (createdDate.isSame(yesterdayDate, Constant.DAY_TYPE_STRING)) {
      pipeOutput = Constant.YESTERDAY_STRING;
    } else {
      pipeOutput = createdDate.format(Constant.DATE_FORMAT_STRING);
    }

    return pipeOutput;
  }
}