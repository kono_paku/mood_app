import { Injectable, NgZone } from "@angular/core";
import { Observable as RxObservable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import "rxjs/add/operator/switchMap";
import { screen } from "platform";

import _ = require('lodash');
import firebase = require("nativescript-plugin-firebase");

import { Constant } from '../../app.constant';
import { LocationService } from '../location/location.service';

/**
 * Provide place information by requesting Place API of Google on the asynchronous request.
 */
@Injectable()
export class HttpService {
  /**
   * Inject NgZone for providing http response in an asynchronous way.
   * @param ngZone NgZone
   */
  constructor(
    private ngZone: NgZone,
    private http: HttpClient
  ) { }

  getData(url: string, parameters: object) {
    let targetUrl = '';

    switch (url) {
      case Constant.MAP_API_URL_STRING: {
        targetUrl = url + this.convertLocationObejectIntoString(parameters);
      }
      default: {
        //TO DO: Needed to be implemnted

      }

      return this.http.get(targetUrl);
    }
  }

  convertLocationObejectIntoString(parameters: object) {
    return parameters[Constant.CURRENT_LATITUDE_STRING] + ',' + parameters[Constant.CURRENT_LONGITUDE_STRING]
  }
}