import { Pipe, PipeTransform } from '@angular/core';
import { Constant } from "../app.constant";

/**
 * Convert number value of tpye into string one of activity
 * 
 * @param type number
 * @returns string
 */
@Pipe({ name: 'activityUserLatterPipe' })
export class ActivityUserLatterPipe implements PipeTransform {
  transform(activityUser: any): string {
    // Set initial value for activityUserLatterPipe
    let pipeOutput = "";

    if (activityUser.length <= Constant.ONE_INT) {
      // Do nothing for returning empty string 
    } else if (activityUser.length === Constant.TWO_INT) {
      pipeOutput = activityUser[Constant.ONE_INT].name;
    } else if (activityUser.length > Constant.TWO_INT) {
      pipeOutput = Constant.OTHERS_STRING;
    } else {
      console.log("<< Unexpected Error at ActivityUserLatterPipe ~ >>");
      console.log(activityUser.length);
      console.log(JSON.stringify(activityUser));
      console.log("<< ~ Unexpected Error at ActivityUserLatterPipe >>");
    }

    return pipeOutput;
  }
}