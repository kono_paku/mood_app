import { Injectable, NgZone } from "@angular/core";
import { Observable as RxObservable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import "rxjs/add/operator/switchMap";
import { screen } from "platform";

import _ = require('lodash');
import firebase = require("nativescript-plugin-firebase");

import { Constant } from '../../app.constant';
import { LocationService } from '../location/location.service';
import { HttpService } from "../http/http.service";

/**
 * Provide place information by requesting Place API of Google on the asynchronous request.
 */
@Injectable()
export class MapApiService {
  // Declare variables for MapApiService
  public mapApi$: RxObservable<any>;
  private mapApiObserver: any;
  private mapApiArray;
  private locationObject = {};
  private mapApiUrl = Constant.MAP_API_URL_STRING;

  /**
   * Inject NgZone for providing place information in an asynchronous way.
   * And Create observable for place API
   * @param ngZone NgZone
   */
  constructor(
    private ngZone: NgZone,
    private locationService: LocationService,
    private httpService: HttpService,
  ) {
      this.mapApi$ = RxObservable.create(observer => {
      this.mapApiObserver = observer;
    });
  }

  public getNearPlacesList() {
    this.locationService.getCurrentLocation().then((currentLocation) => {
      console.log("<< currentLocation in getNearPlacesList ~ >> ");
      if (currentLocation) {
        console.dir(currentLocation);
        this.locationObject[Constant.CURRENT_LATITUDE_STRING] = currentLocation.latitude;
        this.locationObject[Constant.CURRENT_LONGITUDE_STRING] = currentLocation.longitude;
        this.httpService.getData(
          this.mapApiUrl,
          this.locationObject
        ).subscribe((response) => {
            console.log("<< Response in getNearPlacesList ~ >> ");
            // Find the name of district
            let formattedAddress = this.getCurrentDistrictName(response);
            // Renew the successful result
            this.ngZone.run(() => {
              this.mapApiObserver.next(
                formattedAddress
                [Constant.FILTERED_INDEX_INT]
                [Constant.FORMATTED_ADDRESS_STRING]
              );
            });
            console.log("<< ~ Response in getNearPlacesList >> ");
          }, (error) => {
            console.log("<< Error in getNearPlacesList ~ >> ");
            console.dir(error);
            console.log("<< ~ Error in getNearPlacesList s>> ");
          });
      } else {
        console.log("<< currentLocation in getNearPlacesList ~ >> ")
      }
    }, (error) => {
      console.log("<< Failed in getNearPlacesList ~ >> ");
      console.dir(error.message);
      console.log("<< ~ Failed in getNearPlacesList >> ");
    });
  }

  public getCurrentDistrictName(response) {
    return _.filter(response[Constant.RESULTS_STRING],
      (result) => {
        const DISTRICT_STRING_ARRAY = [
          Constant.JAPANESE_KU_DISTRICT_STRING,
          Constant.JAPANESE_SHI_DISTRICT_STRING
        ];
        return _.some(DISTRICT_STRING_ARRAY,
          (district_string) => _.includes(result[Constant.FORMATTED_ADDRESS_STRING],district_string));
      });            

  }
}