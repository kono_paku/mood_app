import _ = require('lodash');

export class PostModel {

  /**
   * Properties of post model (summary)
   */
  static readonly PROPERTIES_SUMMARY = [
    'user',
    'title',
    'caption',
    'thumbnail',
    'video',
    'tiimestamp',
    'likesCount',
    'commentsCount'
  ];

  /**
   * Properties of post model (detail)
   */
  static readonly PROPERTIES_DETAIL = _.union(PostModel.PROPERTIES_SUMMARY, [
    'place',
    'statics',
    'starRate'
  ]);

  /**
   * Post ID
   */
  public readonly id: string;

  /**
   * User ID
   */
  public user: string;

  /**
   * Place ID
   */
  public place: string;

  /**
   * Post title
   */
  public title: string;

  /**
   * Post caption
   */
  public caption: string;

  /**
   * Post thumbnail (usually 1st frame of video)
   */
  public thumbnail: {
    /**
     * Thumbnail image URL (GAE magic URL)
     */
    url: string,

    /**
     * Thumbnail dimensions (width)
     */
    width: number,

    /**
     * Tumbnail dimensions (height)
     */
    height: number
  };

  /**
   * Post video
   */
  public video: {
    /**
     * Video source URL (Firebase Storage download URL)
     */
    url: string,

    /**
     * Video dimensions (width)
     */
    width: number,

    /**
     * Video dimensions (height)
     */
    height: number
  };

  /**
   * Statics of place (post owner's opinion about place)
   */
  public statics: { [categoryId: string]: number; };

  /**
   * Star rate of place (post owner's opinion about place)
   */
  public starRate: number;

  /**
   * Published datetime (unix timestamp in milliseconds)
   */
  public timestamp: number;

  /**
   * Count of users who liked this post
   */
  public likesCount: number;

  /**
   * Count of comments to this post
   */
  public commentsCount: number;

  constructor(
    id: string
  ) {
    this.id = id;
  }

}
