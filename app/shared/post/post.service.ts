import { Injectable, NgZone } from '@angular/core';
import { Observable as RxObservable } from 'rxjs/Observable';
import { screen } from 'platform';

import _ = require('lodash');
import firebase = require('nativescript-plugin-firebase');

import { UserService } from '../user/user.service';

import { PostModel } from './post.model';
import { Constant } from '../../app.constant';

@Injectable()
export class PostService {

  /**
   * Map of post models
   */
  readonly posts$: RxObservable<{ [postId: string]: PostModel; }>;

  private posts: { [postId: string]: PostModel; } = {};
  private postsObserver;

  /**
   * Timeline posts IDs
   */
  readonly timelinePosts$: RxObservable<Array<string>>;

  private timelinePosts: Array<string> = [];
  private timelinePostsObserver;

  constructor(
    private ngZone: NgZone,
    private userService: UserService
  ) {
    this.posts$ = RxObservable.create(observer => {
      this.postsObserver = observer;
      this.postsObserver.next(this.posts);
    });

    this.timelinePosts$ = RxObservable.create(observer => {
      this.timelinePostsObserver = observer;
      this.timelinePostsObserver.next(this.timelinePosts);
    });
  }

  /**
   * Read post and related data.
   *
   * @param postId Post ID
   * @param propertiesToRead `PostModel` properties to read
   */
  public readPost(postId: string, propertiesToRead = PostModel.PROPERTIES_DETAIL): void {
    const post = this.posts[postId] || new PostModel(
      postId
    );

    const readPropertyPromises = [];

    _.each(propertiesToRead, (property) => {
      if (_.isUndefined(post[property])) {
        readPropertyPromises.push(firebase.getValue(`/posts/${postId}/${property}`));
      }
    });

    if (_.isEmpty(readPropertyPromises)) return;

    Promise.all(readPropertyPromises).then((results) => {
      _.each(results, (result) => {
        post[result.key] = result.value;
      });

      console.log('<< readPost ~ >>');
      console.dir(post);
      console.log('<< ~ readPost >>');

      this.userService.readUserProfile(post.user, Constant.SUMMARY_TYPE_STRING);

      this.posts[postId] = post;

      this.ngZone.run(() => {
        if (this.postsObserver) {
          this.postsObserver.next(this.posts);
        }
      });
    });
  }

  /**
   * Read timeline posts IDs.
   *
   * @param limit Limits to read
   * @param endAt Start post ID to read (reverse from latest)
   */
  public readTimelinePosts(limit: number = 10, endAt?: string) {
    firebase.getCurrentUser().then((user) => { // FIXME: Move to userService.currentUser
      const handleQuery = (result) => {
        _.each(_.keys(result.value), (postId) => {
          if (!_.includes(this.timelinePosts, postId)) {
            this.timelinePosts.push(postId);
            this.readPost(postId, PostModel.PROPERTIES_SUMMARY);
          }
        });

        this.timelinePosts = _.reverse(_.sortBy(this.timelinePosts));

        console.log('<< readTimelinePosts ~ >>');
        console.dir(this.timelinePosts);
        console.log('<< ~ readTimelinePosts >>');

        this.ngZone.run(() => {
          if (this.timelinePostsObserver) {
            this.timelinePostsObserver.next(this.timelinePosts);
          }
        });
      };

      const queryOptions = {
        singleEvent: true,
        orderBy: {
          type: firebase.QueryOrderByType.KEY,
        },
        ranges: [],
        limit: {
          type: firebase.QueryLimitType.LAST,
          value: limit
        }
      };

      if (endAt) {
        queryOptions.ranges.push({
          type: firebase.QueryRangeType.END_AT,
          value: endAt
        });
      }

      firebase.query(
        handleQuery,
        `/userTimelinePosts/${user.uid}`,
        queryOptions
      );
    });
  }

}