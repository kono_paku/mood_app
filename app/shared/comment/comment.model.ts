/**
 * Class for constructing comment information
 */
export class CommentModel {
  /**
   * Consturct an instance for comment after getting response from Firebase API.
   * @param id string
   * @param user string
   * @param timestamp number
   * @param message string
   */
  constructor(
    public id: string,
    public user: string, 
    public timestamp: number, 
    public message: string) { }
}
