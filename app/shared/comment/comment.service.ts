import { Injectable, NgZone } from "@angular/core";
import { Observable as RxObservable } from 'rxjs/Observable';
import "rxjs/add/operator/switchMap";
import { screen } from "platform";

import _ = require('lodash');
import firebase = require("nativescript-plugin-firebase");

import { CommentModel } from "./comment.model";
import { UserService } from "../user/user.service";
import { Constant } from '../../app.constant';

/**
 * Provide information for comment page based on the asynchronous request.
 */
@Injectable()
export class CommentService {
  // Declare variables for CommentService
  public comments$: RxObservable<any>;
  private commentObserver: any;
  private commentArray;

  /**
   * Inject NgZone and UserService for providing comment and user information in an asynchronous way.
   * And Create observable of comments for providing user information in an asynchronous way
   * @param ngZone NgZone
   * @param userService UserService
   */
  constructor(
    private ngZone: NgZone,
    private userService: UserService
  ) {
    this.comments$ = RxObservable.create(observer => {
      this.commentObserver = observer;
    });
  }

  /**
    * Create comment and user information by using firebase API.
    * 
    * @param postId string
    */
  public createPostComments(postId: string, commentMessage: string) {
    // Set initial value for user who creates the comment message
    let commentUser = '';

    // Find out current login user
    firebase.getCurrentUser().then((successResult) => {
      console.log("<< createPostComments getCurrentUser successResult >>");
      console.log(JSON.stringify(successResult.uid));
      commentUser = successResult.uid;

      // Request to create comment information by the post ID in an asynchronous way
      firebase.push(
        `/postComments/${postId}`,
        {
          message: commentMessage,
          user: commentUser,
          timestamp: firebase.ServerValue.TIMESTAMP,
        }
      ).then((successResult) => {
        // TODO : Needed to handle a event of success on creating request
        console.log('<< createPostComments push successResult >>' + JSON.stringify(successResult));
      }).catch((failureResult) => {
        // TODO : Needed to handle a event of failure on creating request
        console.log('<< createPostComments push failureResult >>' + JSON.stringify(failureResult));
      });
    
    }).catch((failureResult) => {
      console.log("<< createPostComments getCurrentUser failureResult >> ");
      console.log(JSON.stringify(failureResult));
    });
  }

  /**
   * Read comment and user information from firebase.
   * These observable objects will be renewed after getting the response.
   * And it should be delivered to async pipe or subsribed by any observer.
   * 
   * @param postId string
   * @returns Object
   */
  public readPostComments(postId: string) {
    this.commentArray = [];
    // Request to read comment information by the post ID in an asynchronous way
    firebase.query(
      (successResult) => {
        console.log('<< readPostComments successResult ~ >>' + JSON.stringify(successResult));
        
        switch (successResult.type) {
          case Constant.CHILD_REMOVED_TYPE_STRING: {
            _.remove(this.commentArray, (commentElement) => {
              return commentElement.id === successResult.key;
            });
            break;
          }
          default: {
            // Make a variable from the response with key value
            let commentResponse = _.extend(successResult.value, { id: successResult.key });

            // Search that there is the same comment in the array of Comment
            let existedComment = _.find(this.commentArray, (commentElement) => {
              return commentElement.id === commentResponse.id;
            });

            // Input response into an Comment instance
            let commentModel = new CommentModel(
              commentResponse.id,
              commentResponse.user,
              commentResponse.timestamp,
              commentResponse.message
            );

            if (_.isNil(existedComment)) {
              // If the same comment is not existed, 
              // add Comment instance into the array of Comment
              this.commentArray.unshift(commentModel);
            } else {
              // If the same comment is existed,
              // Update Comment instance into the array of Comment
              _.extend(existedComment, commentModel);
            }

            // Request a user information by the user ID of comment in an asynchronous way
            this.userService.readUserProfile(commentModel.user, Constant.SUMMARY_TYPE_STRING);
          }
        }

        // Renew the successful result
        this.ngZone.run(() => {
          this.commentObserver.next(this.commentArray);
        });

        console.log('<< ~ readPostComments successResult >>' + JSON.stringify(successResult));
      },
      // Request to get comment information by post ID from Firebase API
      `/postComments/${postId}/`,
      {
        // TODO : Needed to add comment to explain the orderBy option
        orderBy: {
          type: firebase.QueryOrderByType.KEY,
        },
      }
    ).catch((failureResult) => {
      // TODO : Needed to handle a event of failure on reading request
      console.log('<< readPostComments failureResult ~ >>' + JSON.stringify(failureResult));
      console.log('<< ~ readPostComments failureResult >>' + JSON.stringify(failureResult));
    });
  }

  /**
   * Update comment and user information by using firebase API.
   * 
   * @param postId string
   */
  public updatePostComments(postId: string, commentModel: CommentModel) {
    // Request to update comment information by the post ID in an asynchronous way
    firebase.update(
      `/postComments/${postId}/${commentModel.id}`,
      {
        message: commentModel.message,
        user: commentModel.user,
        timestamp: commentModel.timestamp,
      }
    ).then((successResult) => {
      // TODO : Needed to handle a event of success on updating request
      console.log('<< updatePostComments successResult ~ >>' + JSON.stringify(successResult));
      console.log('<< ~ updatePostComments successResult >>' + JSON.stringify(successResult));
    }).catch((failureResult) => {
      // TODO : Needed to handle a event of failure on updating request
      console.log('<< updatePostComments failureResult ~ >>' + JSON.stringify(failureResult));
      console.log('<< ~ updatePostComments failureResult >>' + JSON.stringify(failureResult));
    });
  }

  /**
   * Update comment and user information by using firebase API.
   * 
   * @param postId string
   */
  public deletePostComments(postId: string, commentModel: CommentModel) {
    // TODO
    firebase.remove(
      `/postComments/${postId}/${commentModel.id}`
    ).then((successResult) => {
      // TODO : Needed to handle a event of success on deleting request
      console.log('<< deletePostComments successResult ~ >>' + JSON.stringify(successResult));
      console.log('<< ~ deletePostComments successResult >>' + JSON.stringify(successResult));
    }).catch((failureResult) => {
      // TODO : Needed to handle a event of failure on deleting request
      console.log('<< deletePostComments failureResult ~ >>' + JSON.stringify(failureResult));
      console.log('<< ~ deletePostComments failureResult >>' + JSON.stringify(failureResult));
    });
  }

}