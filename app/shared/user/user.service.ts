import { Injectable, NgZone } from '@angular/core';
import { Observable as RxObservable } from 'rxjs/Observable';

import _ = require('lodash');
import firebase = require('nativescript-plugin-firebase');

import { UserModel } from './user.model';
import { UserProfileModel } from './user-profile.model';
import { Constant } from '../../app.constant';

@Injectable()
export class UserService {

  // TODO: Current user model
  // readonly currentUser$: RxObservable<UserModel>;

  /**
   * Map of user profile models
   */
  readonly userProfiles$: RxObservable<{ [userId: string]: UserProfileModel; }>;

  private userProfiles: { [userId: string]: UserProfileModel; } = {};
  private userProfilesObserver;

  constructor(private ngZone: NgZone) {
    this.userProfiles$ = RxObservable.create(observer => {
      this.userProfilesObserver = observer;
      this.userProfilesObserver.next(this.userProfiles);
    });
  }

  /**
   * Read user profile.
   *
   * @param userId User ID
   * @param requestType Define the type to read `UserProfileModel` properties
   */
  public readUserProfile(userId: string, requestType: string): void {
    let propertiesToRead = [];

    if (requestType === Constant.DETAIL_TYPE_STRING) {
      propertiesToRead = UserProfileModel.PROPERTIES_DETAIL;
    } else {
      propertiesToRead = UserProfileModel.PROPERTIES_SUMMARY;
    }

    let userProfile = this.userProfiles[userId] || new UserProfileModel(userId);
    let readPropertyPromises = [];

    _.each(propertiesToRead, (property) => {
      if (_.isUndefined(userProfile[property])) {
        readPropertyPromises.push(firebase.getValue(`/users/${userId}/profile/${property}`));
      }
    });

    if (_.isEmpty(readPropertyPromises)) return;

    Promise.all(readPropertyPromises).then((results) => {
      _.each(results, (result) => {
        userProfile[result.key] = result.value;
      });

      console.log('<< readUserProfile ~ >>');
      console.dir(userProfile);
      console.log('<< ~ readUserProfile >>');

      this.userProfiles[userId] = userProfile;

      this.ngZone.run(() => {
        if (this.userProfilesObserver) {
          this.userProfilesObserver.next(this.userProfiles);
        }
      });
    });
  }

}