import _ = require('lodash');

export class UserProfileModel {

  /**
   * Properties of user profile model (summary)
   */
  static readonly PROPERTIES_SUMMARY = [
    'name',
    'photo'
  ];

  /**
   * Properties of user profile model (detail)
   */
  static readonly PROPERTIES_DETAIL = _.union(UserProfileModel.PROPERTIES_SUMMARY, [
    'description',
    'gender'
  ]);

  /**
   * User ID
   */
  public readonly id: string;

  /**
   * Profile name
   */
  public name: string;

  /**
   * Profile photo URL (GAE magic URL)
   */
  public photo: number;

  /**
   * Profile description
   */
  public description: string;

  /**
   * Gender
   */
  public gender: "female" | "male";

  constructor(
    id: string
  ) {
    this.id = id;
  }

}