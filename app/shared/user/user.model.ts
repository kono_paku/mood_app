import { UserProfileModel } from './user-profile.model';

export class UserModel {

  /**
   * User ID
   */
  public readonly id: string;

  /**
   * User profile
   */
  public profile: UserProfileModel;

  // TODO: interests, comments, likes, bookmarks, (my) posts, (timeline) posts, etc.

  constructor(
    id: string
  ) {
    this.id = id;
  }

}
