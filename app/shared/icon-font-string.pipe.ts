import { Pipe, PipeTransform } from '@angular/core';
import { Constant } from "../app.constant";

/**
 * Convert icon font character code to one-letter string.
 *
 * @param charCode number
 * @returns string
 */
@Pipe({ name: 'iconFontString' })
export class IconFontStringPipe implements PipeTransform {
  transform(charCode: number): string {
    return String.fromCharCode(charCode);
  }
}