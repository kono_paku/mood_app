import { Pipe, PipeTransform } from '@angular/core';
import { Constant } from "../app.constant";

/**
 * Convert number value of tpye into string one of activity
 * 
 * @param type number
 * @returns string
 */
@Pipe({ name: 'activityDescriptionPipe' })
export class ActivityDescriptionPipe implements PipeTransform {
  transform(activity: any): string {
    // Set initial value for activityDescriptionPipe
    let pipeOutput = "";

    switch (Number(activity.type)) {
      case Constant.ONE_INT:
        pipeOutput = Constant.ACTIVITY_TYPE_ONE_STRING;
        break;

      case Constant.TWO_INT:
        pipeOutput = Constant.ACTIVITY_TYPE_TWO_FORMER_STRING + activity.postCount + Constant.ACTIVITY_TYPE_TWO_LETTER_STRING;
        break;

      case Constant.THREE_INT:
        pipeOutput = Constant.ACTIVITY_TYPE_THREE_STRING;
        break;

      case Constant.FOUR_INT:
        pipeOutput = Constant.ACTIVITY_TYPE_FOUR_STRING;
        break;

      case Constant.FIVE_INT:
        pipeOutput = Constant.ACTIVITY_TYPE_FIVE_STRING;
        break;

      case Constant.SIX_INT:
        pipeOutput = Constant.ACTIVITY_TYPE_SIX_STRING;
        break;

      default:
        console.log("Unexpected input type at ActivityDescriptionPipe");
        break;
    }

    return pipeOutput;
  }
}