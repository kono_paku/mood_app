import { Injectable, NgZone } from '@angular/core';
import { Observable as RxObservable } from 'rxjs/Observable';
import { screen } from 'platform';
import firebase = require('nativescript-plugin-firebase');

import { Constant } from '../../app.constant';
import { resumeEvent, ApplicationEventData, on as applicationOn } from "application";
import * as geolocation from "nativescript-geolocation";

@Injectable()
export class LocationService {
  // Declare variables for LocationService
  public location$: RxObservable<any>;
  private locationObserver: any;
  private locationObeject = {};

  constructor(private ngZone: NgZone) {
    this.getCurrentLocation();

    this.location$ = RxObservable.create(observer => {
      this.locationObserver = observer;
    });
  }
  
  addResumEventHandler() {
    applicationOn(resumeEvent, (args: ApplicationEventData) => {
      console.log("<< ResumeEvent in applicationOn >>")
      this.checkLocationEnabled();
    });
  }

  checkLocationEnabled() {
    geolocation.isEnabled().then((isEnabled) => {
      console.log("<< isEnabled value in checkLocationEnabled ~ >> ");
      this.ngZone.run(() => {
        this.locationObserver.next(isEnabled);
      });
      console.log("<< ~ isEnabled value in checkLocationEnabled >> ");
    }, (error) => {
      console.log("<< Error to check enable value in handleGlolocationError >> ");
      console.dir(error.message);
    });
  }

  getCurrentLocation() {
    return geolocation.getCurrentLocation({
      desiredAccuracy: Constant.DESIRED_ACCURACY_INT,
      updateDistance: Constant.UPDATE_DISTANCE,
      maximumAge: Constant.MAXIMUM_AGE_INT,
      timeout: Constant.TIME_OUT_INT
    });
  }
}