import { Injectable, NgZone } from "@angular/core";
import { Observable as RxObservable } from 'rxjs/Observable';
import "rxjs/add/operator/switchMap";
import { screen } from "platform";

import _ = require('lodash');
import firebase = require("nativescript-plugin-firebase");

import { LikeModel } from "./like.model";
import { UserService } from "../user/user.service";
import { Constant } from '../../app.constant';

/**
 * Provide information for like page based on the asynchronous request.
 */
@Injectable()
export class LikeService {
  // Declare variables for LikeService
  public likes$: RxObservable<any>;
  private likeObserver: any;
  private likeArray;

  /**
   * Inject NgZone and UserService for providing like and user information in an asynchronous way.
   * And Create observable of likes for providing user information in an asynchronous way
   * @param ngZone NgZone
   * @param userService UserService
   */
  constructor(
    private ngZone: NgZone,
    private userService: UserService
  ) {
    this.likes$ = RxObservable.create(observer => {
      this.likeObserver = observer;
    });
  }

  /**
    * Create like and user information by using firebase API.
    * 
    * @param postId string
    */
  public createPostLikes(postId: string) {
    // Set initial value for user who creates the like message
    let likeUser = '';

    // Find out current login user
    firebase.getCurrentUser().then((successResult) => {
      console.log("<< createPostLikes getCurrentUser successResult ~ >>");
      console.log(JSON.stringify(successResult.uid));
      likeUser = successResult.uid;

      // Request to create comment information by the post ID in an asynchronous way
      firebase.push(
        `/postLikes/${postId}`,
        {
          user: likeUser,
          timestamp: firebase.ServerValue.TIMESTAMP,
        }
      ).then((successResult) => {
        // TODO : Needed to handle a event of success on creating request
        console.log('<< createPostLikes push successResult ~ >>' + JSON.stringify(successResult));
      }).catch((failureResult) => {
        // TODO : Needed to handle a event of failure on creating request
        console.log('<< ~ createPostLikes push failureResult >>' + JSON.stringify(failureResult));
      });
      console.log("<< ~ reatePostLikes getCurrentUser successResult >>");
    }).catch((failureResult) => {
      console.log("<< createPostLikes getCurrentUser failureResult ~ >> ");
      console.log(JSON.stringify(failureResult));
      console.log("<< ~ createPostLikes getCurrentUser failureResult >> ");
    });
  }

  /**
   * Read like and user information from firebase.
   * These observable objects will be renewed after getting the response.
   * And it should be delivered to async pipe or subsribed by any observer.
   * 
   * @param postId string
   * @returns Object
   */
  public readPostLikes(postId: string) {
    this.likeArray = [];
    // Request to read comment information by the post ID in an asynchronous way
    firebase.query(
      (successResult) => {
        console.log('<< readPostLikes successResult ~ >>' + JSON.stringify(successResult));

        switch (successResult.type) {
          case Constant.CHILD_REMOVED_TYPE_STRING: {
            _.remove(this.likeArray, (likeElement) => {
              return likeElement.id === successResult.key;
            });
            break;
          }
          default: {
            // Make a variable from the response with key value
            let likeResponse = _.extend(successResult.value, { id: successResult.key });

            // Search that there is the same comment in the array of Comment
            let existedLike = _.find(this.likeArray, (likeElement) => {
              return likeElement.id === likeResponse.id;
            });

            // Input response into an Like instance
            let likeModel = new LikeModel(
              likeResponse.id,
              likeResponse.user,
              likeResponse.timestamp,
            );

            if (_.isNil(existedLike)) {
              // If the same comment is not existed, 
              // add Comment instance into the array of Comment
              this.likeArray.unshift(likeModel);
            } else {
              // If the same comment is existed,
              // Update Comment instance into the array of Comment
              _.extend(existedLike, likeModel);
            }

            // Request a user information by the user ID of comment in an asynchronous way
            this.userService.readUserProfile(likeModel.user, Constant.SUMMARY_TYPE_STRING);
            break;
          }
        }

        // Renew the successful result
        this.ngZone.run(() => {
          this.likeObserver.next(this.likeArray);
        });
        console.log('<< ~ readPostLikes successResult >>' + JSON.stringify(successResult));
      },
      // Request to get comment information by post ID from Firebase API
      `/postLikes/${postId}/`,
      {
        // TODO : Needed to add comment to explain the orderBy option
        orderBy: {
          type: firebase.QueryOrderByType.KEY,
        },
      }
    ).catch((failureResult) => {
      // TODO : Needed to handle a event of failure on reading request
      console.log('<< readPostLikes failureResult ~ >>' + JSON.stringify(failureResult));
      console.log('<< ~ readPostLikes failureResult >>' + JSON.stringify(failureResult));
    });
  }

  /**
   * Update like and user information by using firebase API.
   * 
   * @param postId string
   */
  public updatePostLikes(postId: string, likeModel: LikeModel) {
    // Request to update comment information by the post ID in an asynchronous way
    firebase.update(
      `/postLikes/${postId}/${likeModel.id}`,
      {
        user: likeModel.user,
        timestamp: likeModel.timestamp,
      }
    ).then((successResult) => {
      // TODO : Needed to handle a event of success on updating request
      console.log('<< updatePostLikes successResult ~ >>' + JSON.stringify(successResult));
      console.log('<< ~ updatePostLikes successResult >>' + JSON.stringify(successResult));
    }).catch((failureResult) => {
      // TODO : Needed to handle a event of failure on updating request
      console.log('<< updatePostLikes failureResult ~ >>' + JSON.stringify(failureResult));
      console.log('<< ~ updatePostLikes failureResult >>' + JSON.stringify(failureResult));
    });
  }

  /**
   * Update comment and user information by using firebase API.
   * 
   * @param postId string
   */
  public deletePostLikes(postId: string, likeModel: LikeModel) {
    // TODO
    firebase.remove(
      `/postLikes/${postId}/${likeModel.id}`
    ).then((successResult) => {
      // TODO : Needed to handle a event of success on deleting request
      console.log('<< deletePostLikes successResult ~ >>' + JSON.stringify(successResult));
    }).catch((failureResult) => {
      // TODO : Needed to handle a event of failure on deleting request
      console.log('<< ~ deletePostLikes failureResult >>' + JSON.stringify(failureResult));
    });
  }

}