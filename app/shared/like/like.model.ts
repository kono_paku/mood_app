/**
 * Class for constructing like information
 */
export class LikeModel {
  /**
   * Consturct an instance for like after getting response from Firebase API.
   * @param id string
   * @param user string
   * @param timestamp number
   */
  constructor(
    public id: string,
    public user: string,
    public timestamp: number) { }
}
