import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, NgZone } from '@angular/core';
import { Observable as RxObservable } from 'rxjs/Observable';
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions, PageRoute } from "nativescript-angular/router";
import { isIOS, screen } from "platform";
import { Page } from 'tns-core-modules/ui/page/page';

import { LikeService } from "../../shared/like/like.service";
import { UserService } from "../../shared/user/user.service";
import { Constant } from "../../app.constant";

import { isAndroid } from "platform";

@Component({
  moduleId: module.id,
  selector: 'app-post-likes',
  templateUrl: './post-likes.component.html',
  styleUrls: ['./post-likes.component.css']
})
export class PostLikesComponent implements OnInit {

  // Declare variables for PostLikesComponent
  private postId: string;
  private likes$: RxObservable<any>;
  private userProfiles$: RxObservable<any>;

  /**
   * Inject LikeService, UserService and Routing instances
   * for providing like information and post ID from a request.
   * 
   * @param commentService LikeService
   * @param userService UserService
   * @param activatedRoute ActivatedRoute
   * @param routerExtensions RouterExtensions
   * @param pageRoute PageRoute
   */
  constructor(
    private page: Page,
    private likeService: LikeService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private pageRoute: PageRoute,
  ) {
    // Set up parameters for post id from the routerç
    this.pageRoute.activatedRoute
      .switchMap(activatedRoute => activatedRoute.params)
      .forEach(params => {
        this.postId = params[Constant.POST_ID_PARAMETER_STRING];
      });

    // Set up asynchronous requests for comment and user information for comment page
    this.likes$ = this.likeService.likes$;
    this.userProfiles$ = this.userService.userProfiles$;
  }


  /**
   * Get asynchronous requests for comment and user information for comment page
   */
  ngOnInit() {
    this.likeService.readPostLikes(this.postId);
  }

  ngAfterViewInit() {
    if (isIOS) {
      // Workaround for iOS ScrollView jumping up bug
      // https://stackoverflow.com/questions/47273004/why-is-the-scrollview-jumping-up-and-down-when-returning-to-main-view-on-ios
      this.page.style.marginTop = -20;
    }
  }
}