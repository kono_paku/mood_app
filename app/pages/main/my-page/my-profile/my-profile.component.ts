import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  public user = {
    "name" : "Kono Paku",
    "description": "He strongly argues that the test of implementation should not be underestimated.",
    "photo": "https://lh4.googleusercontent.com/-OCYG_JSSUME/AAAAAAAAAAI/AAAAAAAAAAA/AGi4gfwB71iN926ASoYdzwPzefKniacaDg/s96-c/photo.jpg",
    "followers" : 13,
    "following": 14,
    "posts" : 9,
  } 

  constructor() { }

  ngOnInit() { }

}
