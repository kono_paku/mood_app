import { Component, OnInit } from '@angular/core';
import { SegmentedBar, SegmentedBarItem, selectedBackgroundColorProperty } from "ui/segmented-bar";

import firebase = require("nativescript-plugin-firebase");
import { selectedIndexProperty } from 'tns-core-modules/ui/tab-view/tab-view';

@Component({
  moduleId: module.id,
  selector: 'app-my-page',
  templateUrl: './my-page.component.html',
  styleUrls: ['./my-page.component.css']
})
export class MyPageComponent implements OnInit {

  segmentedBarSelectedIndex = 0;

  constructor() { }

  ngOnInit() {
    // firebase.getCurrentUser().then((user) => {
    //   console.log(`current user: ${JSON.stringify(user)}`);
    // }, (error) => {
    //   console.log(`get current user error: ${error}`);
    // });
  }

  public onSignOutTap() {
    firebase.logout();
  }

  public onSegmentedBarIndexChange(args) {
    let segmentedBar = <SegmentedBar>args.object;
    this.segmentedBarSelectedIndex = segmentedBar.selectedIndex;
  }

}
