import { Component, OnInit } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page/page';
import { RouterExtensions, PageRoute } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { SegmentedBar, SegmentedBarItem } from "ui/segmented-bar";

@Component({
  moduleId: module.id,
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {
  public segmentedBarSelectedIndex = 0;

  constructor() { }

  ngOnInit() { }

  public onSegmentedBarIndexChange(args) {
    let segmentedBar = <SegmentedBar>args.object;

    this.segmentedBarSelectedIndex = segmentedBar.selectedIndex;
  }

}
