import { Component, OnInit } from '@angular/core';
import { TouchGestureEventData } from "ui/gestures";
import { Constant } from '../../../../app.constant';

@Component({
  moduleId: module.id,
  selector: 'app-my-activities',
  templateUrl: './my-activities.component.html',
  styleUrls: ['./my-activities.component.css']
})
export class MyActivitiesComponent implements OnInit {
  public xPosition: number;
  public yPosition: number;

  public activities = [
    {
      "type": "2",
      "postCount": "6",
      "comment": "",
      "user": [{
        "name": "Shindoek Kang",
        "photo": "https://lh4.googleusercontent.com/-FcShbl0ixwo/AAAAAAAAAAI/AAAAAAAAEXE/Wm_E-TjQrRQ/s96-c/photo.jpg"
      }]
    },
    {
      "type": "6",
      "postCount": "",
      "comment": "Does the restaurant serve nice food as well?",
      "user": [{
        "name": "Shindoek Kang",
        "photo": "https://lh4.googleusercontent.com/-FcShbl0ixwo/AAAAAAAAAAI/AAAAAAAAEXE/Wm_E-TjQrRQ/s96-c/photo.jpg"
      }]
    },
    {
      "type": "3",
      "postCount": "",
      "comment": "",
      "user": [{
        "name": "yummjanus",
        "photo": "https://lh4.googleusercontent.com/-OCYG_JSSUME/AAAAAAAAAAI/AAAAAAAAAAA/AGi4gfwB71iN926ASoYdzwPzefKniacaDg/s96-c/photo.jpg"
      }]
    },
    {
      "type": "3",
      "postCount": "",
      "comment": "",
      "user": [{
        "name": "snowyuki",
        "photo": "https://lh6.googleusercontent.com/-69D93xQV-9E/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWCmk57P4uQo1nb84BIW1zYqy4Ydtg/s96-c/photo.jpg"
      }]
    },
    {
      "type": "1",
      "postCount": "",
      "comment": "",
      "user": [{
        "name": "yummjanus",
        "photo": "https://lh4.googleusercontent.com/-OCYG_JSSUME/AAAAAAAAAAI/AAAAAAAAAAA/AGi4gfwB71iN926ASoYdzwPzefKniacaDg/s96-c/photo.jpg"
      }]
    },
    {
      "type": "2",
      "postCount": "10",
      "comment": "",
      "user": [{
        "name": "snowyuki",
        "photo": "https://lh6.googleusercontent.com/-69D93xQV-9E/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWCmk57P4uQo1nb84BIW1zYqy4Ydtg/s96-c/photo.jpg"
      },
      {
        "name": "yummjanus",
        "photo": "https://lh4.googleusercontent.com/-OCYG_JSSUME/AAAAAAAAAAI/AAAAAAAAAAA/AGi4gfwB71iN926ASoYdzwPzefKniacaDg/s96-c/photo.jpg"
      },
      {
        "name": "Shindoek Kang",
        "photo": "https://lh4.googleusercontent.com/-FcShbl0ixwo/AAAAAAAAAAI/AAAAAAAAEXE/Wm_E-TjQrRQ/s96-c/photo.jpg"
      }]
    },
    {
      "type": "4",
      "postCount": "",
      "comment": "I've seen it! It is really great to visit.",
      "user": [{
        "name": "yummjanus",
        "photo": "https://lh4.googleusercontent.com/-OCYG_JSSUME/AAAAAAAAAAI/AAAAAAAAAAA/AGi4gfwB71iN926ASoYdzwPzefKniacaDg/s96-c/photo.jpg"
      }]
    },
    {
      "type": "5",
      "postCount": "",
      "comment": "@kono.paku Same as well!",
      "user": [{
        "name": "snowyuki",
        "photo": "https://lh6.googleusercontent.com/-69D93xQV-9E/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWCmk57P4uQo1nb84BIW1zYqy4Ydtg/s96-c/photo.jpg"
      }]
    },
    {
      "type": "4",
      "postCount": "",
      "comment": "Great atmosphere as well as delicious meal!",
      "user": [{
        "name": "snowyuki",
        "photo": "https://lh6.googleusercontent.com/-69D93xQV-9E/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWCmk57P4uQo1nb84BIW1zYqy4Ydtg/s96-c/photo.jpg"
      }]
    },
    {
      "type": "3",
      "postCount": "",
      "comment": "",
      "user": [
        {
          "name": "Shindoek Kang",
          "photo": "https://lh4.googleusercontent.com/-FcShbl0ixwo/AAAAAAAAAAI/AAAAAAAAEXE/Wm_E-TjQrRQ/s96-c/photo.jpg"
        },
        {
          "name": "snowyuki",
          "photo": "https://lh6.googleusercontent.com/-69D93xQV-9E/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWCmk57P4uQo1nb84BIW1zYqy4Ydtg/s96-c/photo.jpg"
        },
        {
          "name": "yummjanus",
          "photo": "https://lh4.googleusercontent.com/-OCYG_JSSUME/AAAAAAAAAAI/AAAAAAAAAAA/AGi4gfwB71iN926ASoYdzwPzefKniacaDg/s96-c/photo.jpg"
        }
      ]

    },
  ];

  constructor() { }

  ngOnInit() { }

  public onTouchFollow(args: TouchGestureEventData) {
    if (args.action === Constant.BUTTON_DOWN_STRING) {
      this.setViewClassName(args, Constant.BUTTON_CSS_FOLLOW_ACTIVE_STRING);
      this.xPosition = args.getX();
      this.yPosition = args.getY();
    } else if (args.action === Constant.BUTTON_UP_STRING
      && !this.isOutOfBoundary(this.yPosition, args.getY())) {
      this.setViewClassName(args, Constant.BUTTON_CSS_FOLLOW_NORMAL_STRING);
    } else if (args.action === Constant.BUTTON_MOVE_STRING
      && this.isOutOfBoundary(this.yPosition, args.getY())) {
      this.setViewClassName(args, Constant.BUTTON_CSS_FOLLOW_NORMAL_STRING);
    }
  }

  public setViewClassName(targetViewArgs: TouchGestureEventData, cssClass: string) {
    targetViewArgs.view.className = cssClass;
  }

  public isOutOfBoundary(downY: number, currentY: number) {
    let returnValue = false;

    if (Math.abs(currentY - downY) >= 50) {
      returnValue = true;
    } else {
      returnValue = false;
    }

    return returnValue;

  }

  public getAndText(user: any) {
    let returnValue = Constant.EMPTY_STRING;

    if (user.length > Constant.ONE_INT) {
      returnValue = Constant.AND_STRING;
    }

    return returnValue;
  }
}
