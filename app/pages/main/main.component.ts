import { Component, OnInit, OnDestroy } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { TabView, SelectedIndexChangedEventData } from "ui/tab-view";

import { LocationService } from '../../shared/location/location.service';
import { Constant } from '../../app.constant';

//------------------------------------------------------------------------------
//
//  Class
//
//------------------------------------------------------------------------------

@Component({
  moduleId: module.id,
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {

  //--------------------------------------
  //  Properties
  //--------------------------------------

  private tabViewSelectedIndex: number;

  //--------------------------------------
  //  Constructor
  //--------------------------------------

  constructor(
    private routerExtensions: RouterExtensions,
    private locationService: LocationService
  ) { }

  //--------------------------------------
  //  Lifecycle hooks
  //--------------------------------------

  ngOnInit() { }

  ngOnDestroy() { }

  //--------------------------------------
  //  Event handlers
  //--------------------------------------

  public onTabViewIndexChange(args: SelectedIndexChangedEventData) {
    let tabView = <TabView>args.object;

    switch (tabView.selectedIndex) {
      case Constant.HOME_COMPONENT_INDEX_INT: {
        console.log("<< Home tab index in onTabViewIndexChange >>");
        break;
      }

      case Constant.SEARCH_COMPONENT_INDEX_INT: {
        console.log("<< Search tab index in onTabViewIndexChange >>");
        break;
      }

      case Constant.ADD_POST_COMPONENT_INDEX_INT: {
        console.log("<< Add post tab index in onTabViewIndexChange >>");
        // Tab item "+" tapped

        this.routerExtensions.navigate(['/shoot-video'], {
          clearHistory: false,
          transition: {
          name: 'flip'
          }
        });

        // Restore selected index
        tabView.selectedIndex = this.tabViewSelectedIndex;
        break;
      }

      case Constant.ACTIVITY_COMPONENT_INDEX_INT: {
        console.log("<< Activity tab index in onTabViewIndexChange >>");
        break;
      }

      case Constant.MY_PAGE_COMPONENT_INDEX_INT: {
        console.log("<< My page tab index in onTabViewIndexChange >>");
        break;
      }

      default: {
        console.log("<< Unexpected tab index in onTabViewIndexChange >>");
      }
    }

    // Store current selected index
    this.tabViewSelectedIndex = tabView.selectedIndex;
  }

}
