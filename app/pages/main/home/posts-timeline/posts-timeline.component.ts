import { Component, OnInit, AfterViewInit, ChangeDetectionStrategy, NgZone } from '@angular/core';
import { Observable as RxObservable } from 'rxjs/Observable';
import { FlexboxLayout } from "tns-core-modules/ui/layouts/flexbox-layout";
import { RouterExtensions } from "nativescript-angular/router";
import { screen } from "platform";

import _ = require('lodash');
import firebase = require('nativescript-plugin-firebase');

import { UserService } from '../../../../shared/user/user.service';
import { PostService } from '../../../../shared/post/post.service';

import { PostModel } from '../../../../shared/post/post.model';

@Component({
  moduleId: module.id,
  selector: 'app-posts-timeline',
  templateUrl: './posts-timeline.component.html',
  styleUrls: ['./posts-timeline.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostsTimelineComponent implements OnInit {

  public timelinePosts$: RxObservable<Array<string>>;
  public posts$: RxObservable<{ [postId: string]: PostModel; }>;
  public userProfiles$: RxObservable<Object>;

  public videoContainerWidth: number;
  public videoContainerHeight: number;

  constructor(
    private ngZone: NgZone,
    private routerExtensions: RouterExtensions,
    private userService: UserService,
    private postService: PostService
  ) {
    this.timelinePosts$ = postService.timelinePosts$;
    this.posts$ = postService.posts$;
    this.userProfiles$ = userService.userProfiles$;
  }

  ngOnInit() {
    // FIXME: Adjust container width within screen horizontal margin (30)
    this.videoContainerWidth = screen.mainScreen.widthDIPs - 30;

    // FIXME: Adjsut container height within video's original height
    this.videoContainerHeight = this.videoContainerWidth;

    this.postService.readTimelinePosts();
  }

  loadMoreItems() {
    // TODO: this.postService.retrieveTimelinePosts(10, <lastPostId>);
  }

  public onItemTap(args) {
    this.timelinePosts$.subscribe(timelinePosts => {
      const tappedItem = timelinePosts[args.index];

      console.log('On item tap', JSON.stringify(tappedItem));

      const postId = tappedItem;

      this.ngZone.run(() => {
        this.routerExtensions.navigate([`/post-detail/${postId}`], { clearHistory: false });
      });
    });
  }

}