import { Component, OnInit } from '@angular/core';

import * as geolocation from "nativescript-geolocation";
import * as dialogs from "ui/dialogs";
import * as utils from "utils/utils";
import { Constant } from "../../../../app.constant"

@Component({
  moduleId: module.id,
  selector: 'app-search-settings',
  templateUrl: './search-settings.component.html',
  styleUrls: ['./search-settings.component.css']
})
export class SearchSettingsComponent implements OnInit {
  public searchGuideMessage = Constant.SEARCH_GUIDE_MESSAGE_STRING;

  constructor() { }

  ngOnInit() { }

  buttonGetLocationTap(args) {
    // TODO : Needed to implement for android
    this.showDialLog();
  }

  showDialLog() {
    dialogs.confirm({
      title: Constant.EMPTY_STRING,
      message: Constant.ALLOW_ACCESS_TO_LOCATION_STRING,
      okButtonText: Constant.SETTINGS_STRING,
      cancelButtonText: Constant.CANCEL_STRING
    }).then((isOkayButtonPushed) => {
      console.log("<< isOkayButtonPushed in showDialLog ~ >> ");
      console.dir(isOkayButtonPushed);

      if (isOkayButtonPushed) {
        utils.openUrl(Constant.SETTING_URL_STRING);
      }
      console.log("<< ~isOkayButtonPushed in showDialLog >> ");
    }, (isCancelButtonPushed) => {
      console.log("<< isCancelButtonPushed in showDialLog ~ >> ");
      console.dir(isCancelButtonPushed);

      console.log("<< ~isCancelButtonPushed in showDialLog >> ");
    });
  }
}
