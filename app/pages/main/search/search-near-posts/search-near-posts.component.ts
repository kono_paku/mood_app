import { Component, OnInit } from '@angular/core';
import { Observable as RxObservable } from 'rxjs/Observable';

import * as geolocation from "nativescript-geolocation";
import * as dialogs from "ui/dialogs";
import * as utils from "utils/utils";
import { Constant } from "../../../../app.constant"
import { MapApiService } from '../../../../shared/google/map-api.service';

@Component({
  moduleId: module.id,
  selector: 'app-search-near-posts',
  templateUrl: './search-near-posts.component.html',
  styleUrls: ['./search-near-posts.component.css']
})
export class SearchNearPostsComponent implements OnInit {
  public posts = [
    {
      "caption": "What a lovely place",
      "commentsCount": 3,
      "likesCount": 5,
      "place": "place-7",
      "thumbnail": {
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-7%2FIMG_1296.jpg?alt=media&token=a123f657-1c29-455a-8e41-1b0bfa1317ae"
      },
      "timestamp": 1514209031530,
      "title": "Delicious food!",
      "user": "R5LVr0Bt7mMlCNYReUYmoqlm3jP2",
      "video": {
        "height": 720,
        "url": "temp3",
        "width": 480
      }
    },
    {
      "caption": "Not really impressed than what I expected. But worth visiting once.",
      "commentsCount": 2,
      "likesCount": 6,
      "place": "place-8",
      "thumbnail": {
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-8%2FIMG_1352.JPG?alt=media&token=156a5bf4-494d-4f95-bb82-cb9f213805c2"
      },
      "timestamp": 1514209031520,
      "title": "Very cozy place!",
      "user": "R5LVr0Bt7mMlCNYReUYmoqlm3jP2",
      "video": {
        "height": 900,
        "url": "temp4",
        "width": 600
      }
    },
    {
      "caption": "It is a pretty quiet place. I'd like to go again.",
      "commentsCount": 1,
      "likesCount": 1,
      "place": "place-9",
      "thumbnail": {
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-9%2FScreen%20Shot%202018-05-03%20at%2021.29.05.png?alt=media&token=2732eea4-e22e-492a-9e95-890411171081"
      },
      "timestamp": 1514209031532,
      "title": "Cool place to go!",
      "user": "R5LVr0Bt7mMlCNYReUYmoqlm3jP2",
      "video": {
        "height": 1080,
        "url": "temp2",
        "width": 400
      }
    },
    {
      "caption": "What a nice place to go picnic",
      "commentsCount": 14,
      "likesCount": 15,
      "place": "place-10",
      "thumbnail": {
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-10%2FScreen%20Shot%202018-05-03%20at%2021.29.28.png?alt=media&token=b8436a0c-2513-43f8-a061-7206c53995c6"
      },
      "timestamp": 1514209031500,
      "title": "THE nice gate!",
      "user": "ecyhZQyl35fDQXvHRQ1BwNq0rSx1",
      "video": {
        "height": 400,
        "url": "test",
        "width": 300
      }
    },
    {
      "caption": "If you really enjoy spending your vacation on water or would like to try something new and exciting for the first time, then you can consider this vacation. (Repeat) If you really enjoy spending your vacation on water or would like to try something new and exciting for the first time, then you can consider this vacation.",
      "commentsCount": 456,
      "likesCount": 123,
      "place": "place-1",
      "thumbnail": {
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-1%2Fzushi-serfers.jpg?alt=media&token=5de01970-8517-4ac3-ad4d-2f62b4eddc91"
      },
      "timestamp": 1514209031554,
      "title": "Zushi",
      "user": "R5LVr0Bt7mMlCNYReUYmoqlm3jP2",
      "video": {
        "height": 900,
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-1%2Fzushi-serfers.min-b500000.mp4?alt=media&token=70330998-3ca2-493a-87eb-430e6293bb11",
        "width": 720
      }
    },
    {
      "caption": "Best place for pub crawling!",
      "commentsCount": 5,
      "likesCount": 2,
      "place": "place-5",
      "thumbnail": {
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-5%2FScreen%20Shot%202018-05-03%20at%2021.23.40.png?alt=media&token=b141dd92-c660-481c-a966-809a69c86713"
      },
      "timestamp": 1514209031571,
      "title": "A good memoir",
      "user": "ecyhZQyl35fDQXvHRQ1BwNq0rSx1",
      "video": {
        "height": 960,
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-3%2Fshodoshima-olive-beach-sup.min-b500000.mp4?alt=media&token=ca97b657-238e-4243-a359-85da60428aef",
        "width": 560
      }
    },
    {
      "caption": "Definitely best place for being chilled out.",
      "commentsCount": 7,
      "likesCount": 5,
      "place": "place-6",
      "thumbnail": {
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-6%2F48011194.jpg?alt=media&token=2fa2d0d0-564f-4800-bd96-60e66e746df4"
      },
      "timestamp": 1514209031591,
      "title": "What a nice place",
      "user": "R5LVr0Bt7mMlCNYReUYmoqlm3jP2",
      "video": {
        "height": 1000,
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-3%2Fshodoshima-olive-beach-sup.min-b500000.mp4?alt=media&token=ca97b657-238e-4243-a359-85da60428aef",
        "width": 600
      }
    },
    {
      "caption": "What a beautiful place!",
      "commentsCount": 777,
      "likesCount": 789,
      "place": "place-2",
      "thumbnail": {
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-2%2Fshodoshima-olive-beach-sunset.jpg?alt=media&token=9bd59197-31ea-4906-a853-92fe8b24f432"
      },
      "timestamp": 1514209031564,
      "title": "Shodoshima",
      "user": "ecyhZQyl35fDQXvHRQ1BwNq0rSx1",
      "video": {
        "height": 1136,
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-2%2Fshodoshima-olive-beach-sunset.min-b500000.mp4?alt=media&token=2c264196-bf21-419e-8d7c-3fb9896e4ad5",
        "width": 639
      }
    },
    {
      "caption": "What a charming place!",
      "commentsCount": 3,
      "likesCount": 15,
      "place": "place-3",
      "thumbnail": {
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-3%2Fshodoshima-olive-beach-sup.jpg?alt=media&token=2d71615e-7f09-48f3-a7df-21a8a3d0357f"
      },
      "timestamp": 1514209031574,
      "title": "Olive Beach",
      "user": "kcFQJjpBEXbcGXudUVGRrrcouNp2",
      "video": {
        "height": 1280,
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-3%2Fshodoshima-olive-beach-sup.min-b500000.mp4?alt=media&token=ca97b657-238e-4243-a359-85da60428aef",
        "width": 720
      }
    },
    {
      "caption": "Very nice place",
      "commentsCount": 2,
      "likesCount": 6,
      "place": "place-4",
      "thumbnail": {
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-4%2FScreen%20Shot%202018-05-03%20at%2021.11.40.png?alt=media&token=9a2dbd52-8748-4ba1-b370-8d083492de46"
      },
      "timestamp": 1514209031564,
      "title": "吉祥寺公園",
      "user": "kcFQJjpBEXbcGXudUVGRrrcouNp2",
      "video": {
        "height": 1280,
        "url": "https://firebasestorage.googleapis.com/v0/b/mood-dev-fd86e.appspot.com/o/posts%2Fpost-3%2Fshodoshima-olive-beach-sup.min-b500000.mp4?alt=media&token=ca97b657-238e-4243-a359-85da60428aef",
        "width": 720
      }
    }
  ]
  
  public mapApi$: RxObservable<any>;

  constructor(private mapApiService: MapApiService) { }

  ngOnInit() {
    this.mapApi$ = this.mapApiService.mapApi$;
    
    this.mapApiService.getNearPlacesList();
  }
}
