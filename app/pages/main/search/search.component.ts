import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Observable as RxObservable } from 'rxjs/Observable';
import { RouterExtensions } from "nativescript-angular/router";
import { platformNativeScriptDynamic } from "nativescript-angular/platform";
import { Constant } from '../../../app.constant';
import { LocationService} from '../../../shared/location/location.service';
import { MapApiService } from '../../../shared/google/map-api.service';

@Component({
  moduleId: module.id,
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  public location$: RxObservable<any>;

  constructor(
    private routerExtensions: RouterExtensions,
    private locationService: LocationService
  ) { }

  ngOnInit() {
    //
    this.location$ = this.locationService.location$;
    
    //
    this.locationService.getCurrentLocation();

    //
    this.locationService.checkLocationEnabled();

    // 
    this.locationService.addResumEventHandler();


  }
}