import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  constructor() { }

  ngOnInit() { }

}
