import { Component, ViewChild, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import { TextView } from 'nativescript-angular/forms/value-accessors';
import { Page } from 'tns-core-modules/ui/page/page';

import firebase = require("nativescript-plugin-firebase");

//------------------------------------------------------------------------------
//
//  Class
//
//------------------------------------------------------------------------------

@Component({
  moduleId: module.id,
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit, OnDestroy {

  //--------------------------------------
  //  Properties
  //--------------------------------------

  @ViewChild("descriptionTextView") descriptionTextView: ElementRef;

  public isBusy = true;

  public currentUser;

  public photo = '';

  public gender = '';

  public name = '';

  public description = '';

  public currentDescription = '';

  public textViewLineHeight = 18;

  public descriptionTextViewHeight = this.textViewLineHeight * 5; // 2 lines are bottom margine for "restrict max lines" workaround

  //--------------------------------------
  //  Constructor
  //--------------------------------------

  constructor(
    private page: Page,
    private routerExtensions: RouterExtensions
  ) {
    page.actionBarHidden = true;
  }

  //--------------------------------------
  //  Lifecycle hooks
  //--------------------------------------

  ngOnInit() {
    console.log('<< ngInit >>');

    firebase.getCurrentUser().then((user) => {
      console.log('<< currentUser ~ >>');
      console.dir(user);
      console.log('<< ~ currentUser >>');

      this.currentUser = user;

      firebase.getValue(`/users/${user.uid}/profile`).then((result) => {
        console.log('<< currentUser profile ~ >>');
        console.dir(result);
        console.log('<< ~ currentUser profile >>');

        const profile = result.value;

        if (profile) {
          this.photo = profile.photo;
          this.gender = profile.gender;
          this.name = profile.name;
          this.description = profile.description;
        } else {
          this.photo = user.profileImageURL;
          this.name = user.name;
        }

        this.isBusy = false;
      });

    }, (error) => {
      console.error(error);

      this.isBusy = false;
    });
  }

  ngOnDestroy() {
    console.log('<< ngDestroy >>');
  }

  //--------------------------------------
  //  Event handlers
  //--------------------------------------

  public onGenderFemaleButtonTap(event) {
    this.gender = this.gender === 'female' ? '' : 'female';
  }

  public onGenderMaleButtonTap(event) {
    this.gender = this.gender === 'male' ? '' : 'male';
  }

  public onDescriptionTextViewTextChange(event) {
    const textView = <TextView>this.descriptionTextView.nativeElement;

    // FIXME: Tricky workaround to restrict max lines
    if (textView.ios.contentSize.height <= this.descriptionTextViewHeight - this.textViewLineHeight * 2) {
      this.currentDescription = textView.text;
    } else {
      this.description = this.currentDescription;
    }
  }

  public onNextButtonTap(event) {
    this.isBusy = true;

    // TODO: Upload photo to Firebase Storage

    firebase.update(`/users/${this.currentUser.uid}/profile`, {
      name: this.name,
      photo: this.photo,
      gender: this.gender,
      description: this.description
    }).then(() => {
      this.isBusy = false;

      this.routerExtensions.navigate(['/choose-interests'], {
        clearHistory: false
      });
    }, (error) => {
      this.isBusy = false;

      console.error(error);
    });

  }

}
