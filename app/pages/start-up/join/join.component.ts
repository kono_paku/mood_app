import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';

import firebase = require("nativescript-plugin-firebase");

const dialogs = require("ui/dialogs");

//------------------------------------------------------------------------------
//
//  Class
//
//------------------------------------------------------------------------------

@Component({
  moduleId: module.id,
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.scss']
})
export class JoinComponent implements OnInit, OnDestroy {

  //--------------------------------------
  //  Properties
  //--------------------------------------

  public isBusy = false;

  public viewMode = 'signIn';

  public email = '';

  public password = '';

  //--------------------------------------
  //  Constructor
  //--------------------------------------

  constructor(
    private ngZone: NgZone,
    private routerExtensions: RouterExtensions
  ) { }

  //--------------------------------------
  //  Lifecycle hooks
  //--------------------------------------

  ngOnInit() {
    console.log('<< ngInit >>');
  }

  ngOnDestroy() {
    console.log('<< ngDestroy >>');
  }

  //--------------------------------------
  //  Event handlers
  //--------------------------------------

  public onEmailTextFieldChange(event) {
    console.log('<< emailTextField change >>');

    this.email = event.value
  }

  public onPasswordTextFieldChange(event) {
    console.log('<< passwordTextField change >>');

    this.password = event.value
  }

  public onSignInTap() {
    console.log('<< signIn tap >>');

    this.isBusy = true;

    firebase.login({
      type: firebase.LoginType.PASSWORD,
      passwordOptions: {
        email: this.email,
        password: this.password
      }
    }).then((user) => {
      console.log('<< emailPasswordLogin result ~ >>');
      console.dir(user);
      console.log('<< ~ emailPasswordLogin result >>');
    }, (errorMessage) => {
      console.error(`<< emailPasswordLogin error ("${errorMessage}") >>`);

      this.isBusy = false;

      dialogs.alert({
        title: 'Email-Password login error',
        message: errorMessage,
        okButtonText: 'Close'
      });
    });
  }

  public onSignUpTap() {
    console.log('<< signUp tap >>');

    // TODO: Create new email/password account
    // this.isBusy = true;
  }

  public onJoinWithFacebookTap() {
    console.log('<< joinWithFacebook tap >>');

    this.isBusy = true;

    firebase.login({
      type: firebase.LoginType.FACEBOOK,
      facebookOptions: {
        // defaults to ['public_profile', 'email']
        scope: ['public_profile', 'email']
      }
    }).then((user) => {
      console.log(`Facebook login result (user): ${JSON.stringify(user)}`);

      console.log('<< facebookLogin result ~ >>');
      console.dir(user);
      console.log('<< ~ facebookLogin result >>');

    }, (errorMessage) => {
      console.error(`<< facebookLogin error ("${errorMessage}") >>`);

      this.isBusy = false;

      if (errorMessage != 'login cancelled') {
        dialogs.alert({
          title: 'Facebook login error',
          message: errorMessage,
          okButtonText: 'Close'
        });
      }
    });
  }

  public onJoinWithGoogleTap() {
    console.log('<< joinWithGoogle tap >>');

    this.isBusy = true;

    firebase.login({
      type: firebase.LoginType.GOOGLE
    }).then((user) => {
      // Google Sign-In result (user) sample
      // {
      //   "uid": "R5LVr0Bt7mMlCNYReUYmoqlm3jP2",
      //   "anonymous": false,
      //   "providers": [
      //     {
      //       "id": "google.com"
      //     }
      //   ],
      //   "profileImageURL": "https://lh4.googleusercontent.com/-FcShbl0ixwo/AAAAAAAAAAI/AAAAAAAAEXE/Wm_E-TjQrRQ/s96-c/photo.jpg",
      //   "email": "heavymery@gmail.com",
      //   "emailVerified": true,
      //   "name": "Shindeok Kang",
      //   "phoneNumber": null,
      //   "refreshToken": "AEoYo8sZrWLdxQFEhHsAlukQYLyH2u2Ct9eu14CG3LujUnRL7qZ_QC9wFE9JxJCIER0T4FdMgP4BGmJUcJBiDuI3qO1H-zbCjLQDiBc5wA6__IhNOsc5C8vpzeHW7m5jnVusuLfz6vo_Zk3ofwrh9MR6zAX3MzaTSLoz6CK8R_j5YUNoPHfLYDZ6QZ6n24f4IuTUNfrMWdbacgZElweQv5FQKfGi1EvFSg6fU8tkUTUKrfG5Utx8_5TLtAbs5O_Fxc0BGHI8aj9b3r6OjKIGcGhFGm5PXs3t_qYtlFzOQSea8klMFM9TIDSSw4PsQRKZKxtVC_EcaJiFzENFzjV_Mcjh_rx5QyXsJ-bONq45KD50hFtIzFxZcEM"
      // }
      console.log('<< googleSignIn result ~ >>');
      console.dir(user);
      console.log('<< ~ googleSignIn result >>');

    }, (errorMessage) => {
      console.error(`<< googleSignIn error ("${errorMessage}") >>`);

      this.isBusy = false;

      if (errorMessage != 'The user canceled the sign-in flow.') {
        dialogs.alert({
          title: 'Google Sign-In error',
          message: errorMessage,
          okButtonText: 'Close'
        });
      }
    });
  }

  public onSwitchToSignUpTap() {
    console.log('<< switchToSignUp tap >>');

    this.viewMode = 'signUp';
  }

  public onSwitchToSignInTap() {
    console.log('<< switchToSignIn tap >>');

    this.viewMode = 'signIn';
  }

  public onSkipTap() {
    console.log('<< skip tap >>');

    this.isBusy = true;

    firebase.login({
      type: firebase.LoginType.ANONYMOUS
    }).then((user) => {
      console.log('<< anonymousLogin result ~ >>');
      console.dir(user);
      console.log('<< ~ anonymousLogin result >>');

    }, (error) => {
      console.error(error);

      this.isBusy = false;

      dialogs.alert({
        title: 'Anonymous login error',
        message: error,
        okButtonText: 'Close'
      });
    });
  }

}
