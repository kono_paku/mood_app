import { Component, ViewChild, ElementRef, OnInit, OnDestroy, AfterViewInit, NgZone } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Observable as RxObservable } from 'rxjs/Observable';
import { RouterExtensions, PageRoute } from "nativescript-angular/router";
import "rxjs/add/operator/switchMap";
import { isIOS, screen } from "platform";
import { Page } from 'tns-core-modules/ui/page/page';
import { TouchGestureEventData } from "ui/gestures";

import _ = require('lodash');
import firebase = require("nativescript-plugin-firebase");

import { UserService } from '../../shared/user/user.service';
import { PostService } from '../../shared/post/post.service';

import { PostModel } from '../../shared/post/post.model';
import { Constant } from '../../app.constant';

@Component({
  moduleId: module.id,
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit, AfterViewInit, OnDestroy {

  // @ViewChild('videoPlayer') videoPlayer;

  private postId: string;

  public posts$: RxObservable<{ [postId: string]: PostModel; }>;
  public userProfiles$: RxObservable<Object>;

  public videoContainerWidth: number;
  public videoContainerHeight: number;

  public xPosition: number;
  public yPosition: number;

  @ViewChild("commentButton") commentButton: ElementRef;

  constructor(
    private page: Page,
    private ngZone: NgZone,
    private activatedRoute: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private pageRoute: PageRoute,
    private userService: UserService,
    private postService: PostService
  ) {
    this.pageRoute.activatedRoute
      .switchMap(activatedRoute => activatedRoute.params)
      .forEach((params) => {
        this.postId = params["post-id"]
      });

    this.posts$ = postService.posts$;
    this.userProfiles$ = userService.userProfiles$;
  }

  ngOnInit() {
    this.videoContainerWidth = screen.mainScreen.widthDIPs;

    // FIXME: Adjsut container height within video's original height
    this.videoContainerHeight = this.videoContainerWidth;

    this.postService.readPost(this.postId);
  }

  ngAfterViewInit() {
    if (isIOS) {
      // Workaround for iOS ScrollView jumping up bug
      // https://stackoverflow.com/questions/47273004/why-is-the-scrollview-jumping-up-and-down-when-returning-to-main-view-on-ios
      this.page.style.marginTop = -20;
    }
  }

  ngOnDestroy() {
    // this.videoPlayer.nativeElement.destroy();
  }

  // public onVideoPlayerReady() {
  //   console.log('On VideoPlayer ready');

  //   this.videoPlayer.nativeElement.play();
  // }

  // public onVideoPlayerFinished() {
  //   console.log('On VideoPlayer finished');
  // }

  public onLikesTap() {
    this.routerExtensions.navigate([`/post-likes/${this.postId}`], { clearHistory: false });
  }

  public onShowAllCommentsButtonTap(args: TouchGestureEventData) {
    if (args.action === Constant.BUTTON_DOWN_STRING) {
      this.setViewClassName(args, Constant.BUTTON_CSS_ACTIVE_STRING);
      this.xPosition = args.getX();
      this.yPosition = args.getY();
    } else if (args.action === Constant.BUTTON_UP_STRING 
      && !this.isOutOfBoundary(this.yPosition, args.getY())) {
      this.setViewClassName(args, Constant.BUTTON_CSS_NORMAL_STRING);
      this.routerExtensions.navigate([`/post-comments/${this.postId}`], { clearHistory: false });
    } else if (args.action === Constant.BUTTON_MOVE_STRING 
      && this.isOutOfBoundary(this.yPosition, args.getY())) {
      this.setViewClassName(args, Constant.BUTTON_CSS_NORMAL_STRING);
    }
  }

  public setViewClassName(targetViewArgs: TouchGestureEventData, cssClass: string) {
    targetViewArgs.view.className = cssClass;
  }

  public isOutOfBoundary(downY: number, currentY:number) {
    let returnValue = false;

    if (Math.abs(currentY - downY) >= 50) {
      returnValue = true;
    } else {
      returnValue = false;
    }

    return returnValue;

  }

}
