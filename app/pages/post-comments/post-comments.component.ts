import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, NgZone } from '@angular/core';
import { Observable as RxObservable } from 'rxjs/Observable';
import { Page } from 'tns-core-modules/ui/page/page';
import { RouterExtensions, PageRoute } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { isIOS, screen } from "platform";

import { CommentService } from "../../shared/comment/comment.service";
import { UserService } from "../../shared/user/user.service";
import { Constant } from "../../app.constant";

import { TextView } from "ui/text-view";
import { isAndroid } from "platform";

/**
 * Provide a post-comments page based on post id from a request.
 */
@Component({
  moduleId: module.id,
  selector: 'app-post-comments',
  templateUrl: './post-comments.component.html',
  styleUrls: ['./post-comments.component.css']
})
export class PostCommentsComponent implements OnInit {
  // Declare variables for PostCommentsComponent
  private postId: string;
  private comments$: RxObservable<any>;
  private userProfiles$: RxObservable<any>;
  private commentMessage;
  @ViewChild("postButton") postButton: ElementRef;
  @ViewChild("commentTextView") commentTextView: ElementRef;

  /**
   * Inject CommentService, UserService and Routing instances
   * for providing comment information and post ID from a request.
   * 
   * @param commentService CommentService
   * @param userService UserService
   * @param activatedRoute ActivatedRoute
   * @param routerExtensions RouterExtensions
   * @param pageRoute PageRoute
   */
  constructor(
    private page: Page,
    private commentService: CommentService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private pageRoute: PageRoute
  ) {
    // Set up parameters for post id from the routerç
    this.pageRoute.activatedRoute
      .switchMap(activatedRoute => activatedRoute.params)
      .forEach(params => {
        this.postId = params[Constant.POST_ID_PARAMETER_STRING];
      });

    // Set up asynchronous requests for comment and user information for comment page
    this.comments$ = this.commentService.comments$;
    this.userProfiles$ = this.userService.userProfiles$;
  }


  /**
   * Get asynchronous requests for comment and user information for comment page
   */
  ngOnInit() {
    this.commentService.readPostComments(this.postId);
  }

  ngAfterViewInit() {
    if (isIOS) {
      // Workaround for iOS ScrollView jumping up bug
      // https://stackoverflow.com/questions/47273004/why-is-the-scrollview-jumping-up-and-down-when-returning-to-main-view-on-ios
      this.page.style.marginTop = -20;
    }
  }

  onTapTextView(args) {
    if (isIOS) {
      // Workaround for iOS ScrollView jumping up bug
      // https://stackoverflow.com/questions/47273004/why-is-the-scrollview-jumping-up-and-down-when-returning-to-main-view-on-ios
      this.page.style.marginTop = 0;
    }
  }

  textChanged(args) {
    let postButton = this.postButton.nativeElement;
    if (args.length > Constant.TEXT_VIEW_MINIMUM_LENGTH_INT) {
      postButton.isEnabled = true;
    } else {
      postButton.isEnabled = false;
    }
  }

  onTapButton(args) {
    let textView = <TextView>this.commentTextView.nativeElement;

    this.commentService.createPostComments(this.postId, this.commentMessage);
    // TODO : Deleted clearFocus() if dismissSoftInput() is working in Android
    //if (isAndroid) {
    //  textView.android.clearFocus();
    //}

    textView.text = "";
    textView.dismissSoftInput();
  }
}
