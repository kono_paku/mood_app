import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Observable as RxObservable } from 'rxjs/Observable';
import { RouterExtensions } from "nativescript-angular/router";
import { TouchGestureEventData, GestureEventData } from "ui/gestures";
import { isIOS } from "platform";
import { Page } from 'tns-core-modules/ui/page/page';

import _ = require("lodash");

import firebase = require("nativescript-plugin-firebase");

//------------------------------------------------------------------------------
//
//  Class
//
//------------------------------------------------------------------------------

@Component({
  moduleId: module.id,
  selector: 'app-choose-interests',
  templateUrl: './choose-interests.component.html',
  styleUrls: ['./choose-interests.component.scss']
})
export class ChooseInterestsComponent implements OnInit, OnDestroy, AfterViewInit {

  //--------------------------------------
  //  Properties
  //--------------------------------------

  public isBusy = true;

  public currentUser;

  public categories$: RxObservable<Array<any>>;

  public userInterests = {};

  //--------------------------------------
  //  Constructor
  //--------------------------------------

  constructor(
    private page: Page,
    private routerExtensions: RouterExtensions
  ) {
    page.actionBarHidden = true;

    firebase.getCurrentUser().then((user) => {
      this.currentUser = user;
    });

    this.categories$ = RxObservable.create(observer => {
      firebase.getValue('/categories').then((result) => {
        const categories = _.orderBy(_.map(result.value, (item, key) => {
          item.id = key;
          item.selected = false;

          return item;
        }), ['id']);

        observer.next(categories);

        console.log('<< categories ~ >>');
        _.each(categories, (category) => {
          console.dir(category);
        });
        console.log('<< ~ categories >>');

        this.isBusy = false;
      });
    });
  }

  //--------------------------------------
  //  Lifecycle hooks
  //--------------------------------------

  ngOnInit() {
    console.log('<< ngInit >>');
  }

  ngOnDestroy() {
    console.log('<< ngDestroy >>');
  }

  ngAfterViewInit() {
    console.log('<< ngAfterViewInit >>');

    if (isIOS) {
      // Workaround for iOS ScrollView jumping up bug
      // https://stackoverflow.com/questions/47273004/why-is-the-scrollview-jumping-up-and-down-when-returning-to-main-view-on-ios
      this.page.style.marginTop = -20;
    }
  }

  //--------------------------------------
  //  Event handlers
  //--------------------------------------

  public onCategoryListItemTap(category) {
    category.selected = !category.selected;

    this.userInterests[category.id] = category.selected;
  }

  public onBackButtonTap(args: GestureEventData) {
    console.log('<< backButton tap >>');

    this.routerExtensions.back();
  }

   public onNextButtonTap(args: GestureEventData) {
    console.log('<< nextButton tap >>');

    this.isBusy = true;

    console.log('<< userInterests ~ >>');
    console.dir(this.userInterests);
    console.log('<< ~ userInterests >>');

    firebase.update(`/userInterests/${this.currentUser.uid}`, this.userInterests).then(() => {

      // TODO: Build user's timeline by selected category (from cloud function)

      firebase.getValue('/timelinePosts').then((defaultTimelinePostsResult) => {
        firebase.setValue(`/userTimelinePosts/${this.currentUser.uid}`, defaultTimelinePostsResult.value).then(() => {
          this.isBusy = false;

          this.routerExtensions.navigate(['/main'], {
            clearHistory: true,
            transition: {
              name: 'flip'
            }
          });
        }, (error) => {
          console.log(error);
          this.isBusy = false;
        });
      }, (error) => {
        console.log(error);
        this.isBusy = false;
      });

    }, (error) => {
      console.log(error);
      this.isBusy = false;
    });
  }  

}
