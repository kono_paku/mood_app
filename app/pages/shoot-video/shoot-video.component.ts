import { Component, OnInit, OnDestroy, ViewChild, NgZone } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { TouchGestureEventData, GestureEventData } from "ui/gestures";
import { getString, setString } from "application-settings";
import { Page } from 'ui/page';

import _ = require('lodash');

import statusBar = require("nativescript-status-bar");
import { CameraPosition, Quality } from 'nativescript-videorecorder/advanced/advanced-video-view.common';


//------------------------------------------------------------------------------
//
//  Constants
//
//------------------------------------------------------------------------------

const VIEW_MODE_RECORD: string = 'record';

const VIEW_MODE_PREVIEW: string = 'preview';

// FIXME: Default max duration (30 second)
const MAX_RECORD_DURATION: number = 10;

// FIXME: Default min duration (10 second)
const MIN_RECORD_DURATION: number = 3;

const PROGRESS_TIMER_INTERVAL: number = 100;

//------------------------------------------------------------------------------
//
//  Class
//
//------------------------------------------------------------------------------

@Component({
  moduleId: module.id,
  selector: 'app-shoot-video',
  templateUrl: './shoot-video.component.html',
  styleUrls: ['./shoot-video.component.css']
})
export class ShootVideoComponent implements OnInit, OnDestroy {

  //--------------------------------------
  //  Constants
  //--------------------------------------

  public VIEW_MODE_RECORD = VIEW_MODE_RECORD;

  public VIEW_MODE_PREVIEW = VIEW_MODE_PREVIEW;

  public MAX_RECORD_DURATION = MAX_RECORD_DURATION;

  public MIN_RECORD_DURATION = MIN_RECORD_DURATION;

  //--------------------------------------
  //  Properties
  //--------------------------------------

  @ViewChild('videoRecorderAdvanced') videoRecorderAdvanced;

  @ViewChild('previewVideoPlayer') previewVideoPlayer;

  public viewMode: string = VIEW_MODE_RECORD;

  public cameraPosition: CameraPosition = CameraPosition.BACK;

  public quality: Quality = Quality.HIGHEST;

  public isRecording: boolean = false;

  public isMinDurationRecorded: boolean = false;
  public isMaxDurationRecorded: boolean = false;

  public recordProgress: number = 0;

  public recordedVideos: Array<{ duration: number, file: string }> = [];

  public previewVideoIndex: number = -1;

  public previewVideoVolume: number = 1;

  public isPreviewVideoMuted: boolean = false;
  public isPreviewVideoPaused: boolean = false;

  private recordProgressTimer;

  //--------------------------------------
  //  Constructor
  //--------------------------------------

  constructor(
    private page: Page,
    private ngZone: NgZone,
    private routerExtensions: RouterExtensions
  ) {
    page.actionBarHidden = true;
  }

  //--------------------------------------
  //  Lifecycle hooks
  //--------------------------------------

  ngOnInit() {
    console.log('<< ngInit >>');

    statusBar.hide();

    this.cameraPosition = _.defaultTo(getString('cameraPosition'), CameraPosition.BACK);

    this.videoRecorderAdvanced.nativeElement.on('started', args => {
      console.log('<< videoRecorderAdvanced started >>');

      this.isRecording = true;

      this.recordProgressTimer = setInterval(() => {
        console.log(`<< videoRecorderAdvanced progress (duration: ${this.videoRecorderAdvanced.nativeElement.duration}) >>`);

        this.ngZone.run(() => {
          this.recordProgress += PROGRESS_TIMER_INTERVAL / 1000;
        });

        const totalRecordedDuration = _.reduce(this.recordedVideos, function (totalDuration, recordedVideo) {
          return totalDuration + recordedVideo.duration;
        }, 0) + this.videoRecorderAdvanced.nativeElement.duration;

        if (totalRecordedDuration >= MIN_RECORD_DURATION) {
          this.isMinDurationRecorded = true;
        }

        if (totalRecordedDuration >= MAX_RECORD_DURATION) {
          this.isMaxDurationRecorded = true;

          clearInterval(this.recordProgressTimer);

          this.videoRecorderAdvanced.nativeElement.stopRecording();
        }
      }, PROGRESS_TIMER_INTERVAL);
    });

    this.videoRecorderAdvanced.nativeElement.on('finished', args => {
      this.isRecording = false;

      clearInterval(this.recordProgressTimer);

      this.recordProgress = 0;

      const recordedFile = args.object.get('file');

      if (!_.isEmpty(recordedFile)) {
        const recordedVideo = {
          duration: this.videoRecorderAdvanced.nativeElement.duration,
          file: args.object.get('file')
        };

        this.recordedVideos.push(recordedVideo);
      }

      console.log('<< videoRecorderAdvanced finished ~ >>');
      _.each(this.recordedVideos, (recordedVideo) => {
        console.dir(recordedVideo);
      });
      console.log('<< ~ videoRecorderAdvanced finished >>');
    });
  }

  ngOnDestroy() {
    console.log('<< ngDestroy >>');

    this.previewVideoPlayer.nativeElement.pause();

    statusBar.show();
  }

  //--------------------------------------
  //  Methods
  //--------------------------------------

  private switchToPreviewMode() {
    console.log('<< switchToPreviewMode >>');

    this.viewMode = VIEW_MODE_PREVIEW;

    this.previewVideoIndex = this.recordedVideos.length - 1;

    this.previewVideoPlayer.nativeElement.src = this.recordedVideos[this.previewVideoIndex].file;

    this.previewVideoPlayer.nativeElement.setVolume(this.previewVideoVolume);

    this.previewVideoPlayer.nativeElement.play();

    this.isPreviewVideoPaused = false;
  }

  private switchToRecordMode() {
    console.log('<< switchToRecordMode >>');

    this.viewMode = VIEW_MODE_RECORD;

    this.previewVideoPlayer.nativeElement.pause();

    this.previewVideoIndex = -1;
  }

  public playPreviewVideo() {
    const previewVideo = this.recordedVideos[this.previewVideoIndex].file;

    if (this.previewVideoPlayer.nativeElement.src != previewVideo) {
      this.previewVideoPlayer.nativeElement.src = previewVideo;
    }

    this.previewVideoPlayer.nativeElement.play();

    this.isPreviewVideoPaused = false;
  }

  private pausePreviewVideo() {
    this.previewVideoPlayer.nativeElement.pause();

    this.isPreviewVideoPaused = true;
  }

  // public uploadVideo() {

  // }

  //--------------------------------------
  //  Event handlers (record mode)
  //--------------------------------------

  public onToggleCameraButtonTap(args: GestureEventData) {
    console.log('<< toggleCameraButton tap >>');

    this.videoRecorderAdvanced.nativeElement.toggleCamera();

    setString('cameraPosition', this.videoRecorderAdvanced.nativeElement.cameraPosition);
  }

  public onRecordButtonTouch(args: TouchGestureEventData) {
    if (args.action == 'down') {
      console.log('<< shootButton touch(down) >>');

      if (!this.isRecording && !this.isMaxDurationRecorded) {
        this.videoRecorderAdvanced.nativeElement.startRecording();
      }
    } else if (args.action == 'up') {
      console.log('<< shootButton touch(up) >>');

      if (this.isRecording) {
        this.videoRecorderAdvanced.nativeElement.stopRecording();
      }
    }
  }

  public onRetakeButtonTap(args: GestureEventData) {
    console.log('<< retakeButton tap >>');

    // Remove last recorded video
    this.recordedVideos.pop();

    this.isMaxDurationRecorded = false;
  }

  public onForwardButtonTap(args: GestureEventData) {
    console.log('<< forwardButton tap >>');

    this.switchToPreviewMode();
  }

  public onCancelButtonTap(args: GestureEventData) {
    console.log('<< cancelButton tap >>');

    statusBar.show();

    this.routerExtensions.back();
  }

  //--------------------------------------
  //  Event handlers (preview mode)
  //--------------------------------------

  public onPreviewVideoPlayerReady() {
    console.log('<< previewVideoPlayer ready >>');
  }

  public onPreviewVideoPlayerFinished() {
    console.log('<< previewVideoPlayer finish >>');
  }

  public onPreviewVideoMuteButtonTap(args: GestureEventData) {
    console.log('<< previewVideoMuteButton tap >>');

    this.previewVideoPlayer.nativeElement.mute(true);

    this.isPreviewVideoMuted = true;
  }

  public onPreviewVideoUnmuteButtonTap(args: GestureEventData) {
    console.log('<< previewVideoUnmuteButton tap >>');

    this.previewVideoPlayer.nativeElement.mute(false);

    this.isPreviewVideoMuted = false;
  }

  public onPreviewVideoIncreaseVolumeButtonTap(args: GestureEventData) {
    console.log('<< previewVideoIncreaseVolume tap >>');

    if (this.previewVideoVolume >= 1) return;

    this.previewVideoVolume = _.round(this.previewVideoVolume + 0.2, 1);

    console.log(`<< previewVideo set volume (${this.previewVideoVolume}) >>`);

    this.previewVideoPlayer.nativeElement.setVolume(this.previewVideoVolume);
  }

  public onPreviewVideoDecreaseVolumeButtonTap(args: GestureEventData) {
    console.log('<< previewVideoDecreaseVolumeButton tap >>');

    if (this.previewVideoVolume <= 0) return;

    this.previewVideoVolume = _.round(this.previewVideoVolume - 0.2, 1);

    console.log(`<< previewVideo set volume (${this.previewVideoVolume}) >>`);

    this.previewVideoPlayer.nativeElement.setVolume(this.previewVideoVolume);
  }

  public onPreviewVideoPlayButtonTap(args: GestureEventData) {
    this.playPreviewVideo();
  }

  public onPreviewVideoPauseButtonTap(args: GestureEventData) {
    this.pausePreviewVideo();
  }

  public onPreviewVideoPrevButtonTap(args: GestureEventData) {
    if (this.previewVideoIndex == 0) return;

    this.previewVideoIndex = this.previewVideoIndex - 1;

    this.playPreviewVideo();
  }

  public onPreviewVideoNextButtonTap(args: GestureEventData) {
    if (this.previewVideoIndex == this.recordedVideos.length - 1) return;

    this.previewVideoIndex = this.previewVideoIndex + 1;

    this.playPreviewVideo();
  }

  public onFilterEffectButtonTap(args: GestureEventData) {
    console.log('<< filterEffectButton tap >>');

    // TODO: Show filter list, apply filter to selected video (video cut)
  }

  public onRemoveVideoButtonTap(args: GestureEventData) {
    console.log('<< removeVideoButton tap >>');

    _.pullAt(this.recordedVideos, this.previewVideoIndex);

    this.previewVideoIndex = this.previewVideoIndex - 1;

    if (this.previewVideoIndex < 0 || _.size(this.recordedVideos) == 0) {
      this.pausePreviewVideo();

      this.switchToRecordMode();

      return;
    };

    this.playPreviewVideo();
  }

  public onBackButtonTap(args: GestureEventData) {
    console.log('<< backButton tap >>');

    this.switchToRecordMode();
  }

  public onCompleteButtonTap(args: GestureEventData) {
    console.log('<< completeButton tap >>');

    // TODO: Exit video shooting, go to post editing
  }

}
