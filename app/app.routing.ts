import { NgModule } from '@angular/core';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { Routes } from '@angular/router';

import { StartUpComponent } from './pages/start-up/start-up.component';
import { JoinComponent } from './pages/start-up/join/join.component';

import { MainComponent } from './pages/main/main.component';
import { HomeComponent } from './pages/main/home/home.component';
import { PostsTimelineComponent } from './pages/main/home/posts-timeline/posts-timeline.component';
import { SearchComponent } from './pages/main/search/search.component';
import { ActivityComponent } from './pages/main/activity/activity.component';
import { MyActivitiesComponent } from './pages/main/activity/my-activities/my-activities.component';
import { OtherActivitiesComponent } from './pages/main/activity/other-activities/other-activities.component';

import { MyPageComponent } from './pages/main/my-page/my-page.component';
import { MyPostsComponent } from './pages/main/my-page/my-posts/my-posts.component';
import { MyFavoritesComponent } from './pages/main/my-page/my-favorites/my-favorites.component';
import { MyProfileComponent } from './pages/main/my-page/my-profile/my-profile.component';

import { PostDetailComponent } from './pages/post-detail/post-detail.component';
import { PostLikesComponent } from './pages/post-likes/post-likes.component';
import { PostCommentsComponent } from './pages/post-comments/post-comments.component';

import { UserProfileComponent } from './pages/user-profile/user-profile.component';

import { ShootVideoComponent } from './pages/shoot-video/shoot-video.component';
import { EditPostComponent } from './pages/edit-post/edit-post.component';

import { EditProfileComponent } from './pages/edit-profile/edit-profile.component';
import { ChooseInterestsComponent } from './pages/choose-interests/choose-interests.component';

import { SettingsComponent } from './pages/settings/settings.component';

const routes: Routes = [

  // NOTE: route-outlet だと現在のビューの状態が維持できないので、
  // ・状態の維持が必要な場合は page-router-outlet で（画面全体の遷移）
  // ・route-outlet はビューの出し分けや、Segment ビューなどで使えば良さげ

  {
    path: 'start-up', component: StartUpComponent, children: [
      { path: 'join', component: JoinComponent },
      // TODO: Step-by-step guide or feature introduction
    ]
  },

  { path: 'main', component: MainComponent },

  { path: 'post-detail/:post-id', component: PostDetailComponent },
  { path: 'post-likes/:post-id', component: PostLikesComponent },
  { path: 'post-comments/:post-id', component: PostCommentsComponent },

  { path: 'shoot-video', component: ShootVideoComponent },

  { path: 'edit-post', component: EditPostComponent },

  { path: 'user-profile', component: UserProfileComponent },

  // TODO: Separate to FillProfile / EditProfile page (share ProfileForm as component)
  { path: 'edit-profile', component: EditProfileComponent },

  // TODO: Separate to ChooseInterests / EditInterests page (share InterestsList as component)
  { path: 'choose-interests', component: ChooseInterestsComponent },

  { path: 'settings', component: SettingsComponent },
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
