  /**
   * Collect values for configuration in mood-app
   */
export class Constant {

  // Collection of constant values for the application
  static VALUE_CHANGED_TYPE_STRING = 'ValueChanged';
  static CHILD_ADDED_TYPE_STRING = 'ChildAdded';
  static CHILD_CHANGED_TYPE_STRING = 'ChildChanged';
  static CHILD_REMOVED_TYPE_STRING = 'ChildRemoved';

  static DETAIL_TYPE_STRING = 'Detail';
  static SUMMARY_TYPE_STRING = 'Summary';
  
  static DAY_TYPE_STRING: any = 'day';
  static DATE_FORMAT_STRING = 'M.DD.YYYY';
  
  static POST_ID_PARAMETER_STRING = 'post-id';
  
  static TODAY_STRING = 'Today';
  static YESTERDAY_STRING = 'Yesterday';
  
  static EMPTY_STRING = '';
  
  static ALLOW_ACCESS_TO_LOCATION_STRING = 'Please allow access to Location in order to use the location-based services';
  
  static SETTINGS_STRING = 'Settings';
  static CANCEL_STRING = 'Cancel';
  
  static SETTING_URL_STRING = 'app-settings:root';
  
  static SEARCH_GUIDE_MESSAGE_STRING = 'Check out posts around me!';
  
  static CURRENT_LATITUDE_STRING = 'currentLatitude';
  static CURRENT_LONGITUDE_STRING = 'currentLongitude';
  
  static MAP_API_URL_STRING = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAOUGR6BinnRfoEkuxHBqPoSjPEDmpIuAo&language=ja&result_type=locality&latlng=';
  
  static RESULTS_STRING ='results';
  static FORMATTED_ADDRESS_STRING = 'formatted_address';
  
  static JAPANESE_KU_DISTRICT_STRING = '区';
  static JAPANESE_SHI_DISTRICT_STRING = '市';
  
  static BUTTON_UP_STRING = 'up';
  static BUTTON_DOWN_STRING = 'down';
  static BUTTON_MOVE_STRING = 'move';
  static BUTTON_CSS_ACTIVE_STRING = 'btn btn-rounded-lg btn-primary text-uppercase btn-alpha';
  static BUTTON_CSS_NORMAL_STRING = 'btn btn-rounded-lg btn-primary text-uppercase';
  static BUTTON_CSS_FOLLOW_ACTIVE_STRING = 'btn btn-rounded-lg btn-primary btn-follow btn-alpha';
  static BUTTON_CSS_FOLLOW_NORMAL_STRING = 'btn btn-rounded-lg btn-primary btn-follow';
  

  static ACTIVITY_TYPE_ONE_STRING = ' started following you.';
  static ACTIVITY_TYPE_TWO_FORMER_STRING = ' shared ';
  static ACTIVITY_TYPE_TWO_LETTER_STRING = ' posts.';
  static ACTIVITY_TYPE_THREE_STRING = ' liked your posts.';
  static ACTIVITY_TYPE_FOUR_STRING = ' liked your comment:\n';
  static ACTIVITY_TYPE_FIVE_STRING = ' mentioned you in a comment:\n';
  static ACTIVITY_TYPE_SIX_STRING = ' comment:\n';

  static OTHERS_STRING = 'others';
  static AND_STRING = ' and ';

  static ZERO_INT = 0;
  static ONE_INT = 1;
  static TWO_INT = 2;
  static THREE_INT = 3;
  static FOUR_INT = 4;
  static FIVE_INT = 5;
  static SIX_INT = 6;
  
  static MILLISECONDS_CONVERTING_VALUE_INT = 1000;
  static TEXT_VIEW_MINIMUM_LENGTH_INT = Constant.ZERO_INT;
  static YESTERDAY_INDEX_INT: any = Constant.ONE_INT;
  static DESIRED_ACCURACY_INT = Constant.THREE_INT;
  static UPDATE_DISTANCE = 10;
  static MAXIMUM_AGE_INT = 20000;
  static TIME_OUT_INT = 20000;
  static HOME_COMPONENT_INDEX_INT = Constant.ZERO_INT;
  static SEARCH_COMPONENT_INDEX_INT = Constant.ONE_INT;
  static ADD_POST_COMPONENT_INDEX_INT = Constant.TWO_INT;
  static ACTIVITY_COMPONENT_INDEX_INT = Constant.THREE_INT;
  static MY_PAGE_COMPONENT_INDEX_INT = Constant.FOUR_INT;
  static FILTERED_INDEX_INT = Constant.ZERO_INT;

  static ENABLED_BOOLEAN = true;
  static NOT_ENABLED_BOOLEAN = false;
  
}