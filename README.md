# Mood App
Mood iOS/Android app.

>Currently focused on iOS

---

## Installation

We use NativeScript CLI for installation and development. Refer to [NativeScript installation guide](http://docs.nativescript.org/angular/start/quick-setup).

After that, execute command below.

```sh
$ tns install
```

### Setup Firebase

We use Firebase as a back-end and NativeScript plugin for that. 

- [Firebase - Mood Dev](https://console.firebase.google.com/project/mood-dev-fd86e/overview)
- [Firebase - Mood Test](https://console.firebase.google.com/project/mood-test-30b65/overview)
- [Firebase - Mood Stg](https://console.firebase.google.com/project/mood-stg/overview)
- [Firebase - Mood Prod](https://console.firebase.google.com/project/mood-dcacd/overview)

To complete Firebase setup, Follow the instructions on the links below.

1. [NativeScript Firebase plugin - Prerequisites](https://github.com/EddyVerbruggen/nativescript-plugin-firebase#prerequisites)
2. [NativeScript Firebase plugin - Installation](https://github.com/EddyVerbruggen/nativescript-plugin-firebase#installation)

---

## Development

### Run

At first, run our app in connected devices or emulators.

```sh
$ tns run ios|android [--emulator]
```

With the run our app complete, livesync any changed files.

---

## Deploy

Make production build and upload to iTunes Connect.

```sh
tns publish ios <Apple ID> <App-specific(for TNS CLI) password> <Mobile Provisioning Profile(for Distrubution) Identifier(Provision UUID)> <Code Sign Identity>
# e.g. tns publish ios xxxx@xxxx.com xxxx-xxxx-xxxx-xxxx xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx 12X3XX4XX5
```